package com.ccsem;

import com.ccsem.common.CCSEMStorageUnit;
import com.ccsem.common.GetPhase;
import com.ccsem.common.NameData;
import com.ccsem.common.NameSizeData;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.ccsem.App.getPhase;

final class MagnificationServer {
    private List<CCSEMStorageUnit> mag250;
    private List<CCSEMStorageUnit> mag50;
    private CCSEMStorageUnit storageUnit;
    private GetPhase getPhase;

    public void setPartMax(int partMax) {
        this.partMax = partMax;
    }

    private List<NameData> nameselement;
    private List<NameSizeData> nameSizeData;
    private int partMax;
    MagnificationServer(){
        nameselement = new ArrayList<>();
        nameSizeData = new ArrayList<>();
        mag50 = new ArrayList<>();
        mag250 = new ArrayList<>();
        getPhase = new GetPhase();

    }
    List<CCSEMStorageUnit> getMag50(){
        return mag50;

    }
    List<CCSEMStorageUnit> getMag250(){
        return mag250;

    }
    List<NameData> getNameData(){

        return nameselement;
    }
    List<NameSizeData> getNameSizeData(){

        return nameSizeData;
    }

    public int getPartMax() {
        return partMax;
    }

    void initializeMag50Server(File file){
        try {
            XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(file));
            for (Sheet sheet : wb) {
                for (Row row : sheet) {
                    int rowId = row.getRowNum();
                    if (rowId != 0) {
                        storageUnit = new CCSEMStorageUnit();


                        Cell point = row.getCell(0);
                        storageUnit.setPoint((int) point.getNumericCellValue());
                        Cell ctype = row.getCell(1);
                        storageUnit.setCtype((int) ctype.getNumericCellValue());
                        Cell cts = row.getCell(2);
                        storageUnit.setCts((int) cts.getNumericCellValue());
                        Cell na = row.getCell(3);
                        storageUnit.setNA((float) na.getNumericCellValue());
                        Cell mg = row.getCell(4);

                        storageUnit.setMg((float) mg.getNumericCellValue());
                        Cell al = row.getCell(5);
                        storageUnit.setAl((float) al.getNumericCellValue());
                        Cell si = row.getCell(6);
                        storageUnit.setSi(Float.valueOf((float) si.getNumericCellValue()));
                        Cell p = row.getCell(7);
                        storageUnit.setP(Float.valueOf((float) p.getNumericCellValue()));
                        Cell s = row.getCell(8);
                        storageUnit.setS(Float.valueOf((float) s.getNumericCellValue()));
                        Cell cl = row.getCell(9);
                        storageUnit.setCl(Float.valueOf((float) cl.getNumericCellValue()));
                        Cell k = row.getCell(10);
                        storageUnit.setK(Float.valueOf((float) k.getNumericCellValue()));
                        Cell ca = row.getCell(11);
                        storageUnit.setCa(Float.valueOf((float) ca.getNumericCellValue()));
                        Cell fe = row.getCell(12);
                        storageUnit.setFe(Float.valueOf((float) fe.getNumericCellValue()));
                        Cell ba = row.getCell(13);
                        storageUnit.setBa(Float.valueOf((float) ba.getNumericCellValue()));
                        Cell ti = row.getCell(14);
                        storageUnit.setTi(Float.valueOf((float) ti.getNumericCellValue()));
                        Cell xcord = row.getCell(15);
                        storageUnit.setXcoord(Float.valueOf((float) xcord.getNumericCellValue()));
                        Cell ycord = row.getCell(16);
                        storageUnit.setYcoord(Float.valueOf((float) ycord.getNumericCellValue()));
                        Cell avgDia = row.getCell(17);
                        storageUnit.setAvgDiameter(Float.valueOf((float) avgDia.getNumericCellValue()));
                        Cell maxDiameter = row.getCell(18);
                        storageUnit.setMaxDiameter(Float.valueOf((float) maxDiameter.getNumericCellValue()));
                        Cell minDiameter = row.getCell(19);
                        storageUnit.setMinDiameter(Float.valueOf((float) minDiameter.getNumericCellValue()));
                        Cell area = row.getCell(20);
                        storageUnit.setArea(Float.valueOf((float) area.getNumericCellValue()));
                        Cell perim = row.getCell(21);
                        storageUnit.setPerim(Float.valueOf((float) perim.getNumericCellValue()));
                        Cell shape = row.getCell(22);
                        storageUnit.setShape(Float.valueOf((float) shape.getNumericCellValue()));

                        Cell frame = row.getCell(23);
                        storageUnit.setFrame((int) frame.getNumericCellValue());
                        Cell loclib = row.getCell(24);
                        storageUnit.setLocLib((int) loclib.getNumericCellValue());
                        storageUnit.setPhase(getPhase.getPhase(al.getNumericCellValue(), ba.getNumericCellValue(), ca.getNumericCellValue(), cl.getNumericCellValue(), fe.getNumericCellValue(), k.getNumericCellValue(), mg.getNumericCellValue(), na.getNumericCellValue(), p.getNumericCellValue(), s.getNumericCellValue(), si.getNumericCellValue(), ti.getNumericCellValue()));

                        mag50.add(storageUnit);



                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    void initializeMag250Server(File file){
        try {
            XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(file));
            for (Sheet sheet : wb) {
                for (Row row : sheet) {
                    int rowId = row.getRowNum();
                    if (rowId != 0) {
                        storageUnit = new CCSEMStorageUnit();


                        Cell point = row.getCell(0);
                        storageUnit.setPoint((int) point.getNumericCellValue());
                        Cell ctype = row.getCell(1);
                        storageUnit.setCtype((int) ctype.getNumericCellValue());
                        Cell cts = row.getCell(2);
                        storageUnit.setCts((int) cts.getNumericCellValue());
                        Cell na = row.getCell(3);
                        storageUnit.setNA((float) na.getNumericCellValue());
                        Cell mg = row.getCell(4);

                        storageUnit.setMg((float) mg.getNumericCellValue());
                        Cell al = row.getCell(5);
                        storageUnit.setAl((float) al.getNumericCellValue());
                        Cell si = row.getCell(6);
                        storageUnit.setSi(Float.valueOf((float) si.getNumericCellValue()));
                        Cell p = row.getCell(7);
                        storageUnit.setP(Float.valueOf((float) p.getNumericCellValue()));
                        Cell s = row.getCell(8);
                        storageUnit.setS(Float.valueOf((float) s.getNumericCellValue()));
                        Cell cl = row.getCell(9);
                        storageUnit.setCl(Float.valueOf((float) cl.getNumericCellValue()));
                        Cell k = row.getCell(10);
                        storageUnit.setK(Float.valueOf((float) k.getNumericCellValue()));
                        Cell ca = row.getCell(11);
                        storageUnit.setCa(Float.valueOf((float) ca.getNumericCellValue()));
                        Cell fe = row.getCell(12);
                        storageUnit.setFe(Float.valueOf((float) fe.getNumericCellValue()));
                        Cell ba = row.getCell(13);
                        storageUnit.setBa(Float.valueOf((float) ba.getNumericCellValue()));
                        Cell ti = row.getCell(14);
                        storageUnit.setTi(Float.valueOf((float) ti.getNumericCellValue()));
                        Cell xcord = row.getCell(15);
                        storageUnit.setXcoord(Float.valueOf((float) xcord.getNumericCellValue()));
                        Cell ycord = row.getCell(16);
                        storageUnit.setYcoord(Float.valueOf((float) ycord.getNumericCellValue()));
                        Cell avgDia = row.getCell(17);
                        storageUnit.setAvgDiameter(Float.valueOf((float) avgDia.getNumericCellValue()));
                        Cell maxDiameter = row.getCell(18);
                        storageUnit.setMaxDiameter(Float.valueOf((float) maxDiameter.getNumericCellValue()));
                        Cell minDiameter = row.getCell(19);
                        storageUnit.setMinDiameter(Float.valueOf((float) minDiameter.getNumericCellValue()));
                        Cell area = row.getCell(20);
                        storageUnit.setArea(Float.valueOf((float) area.getNumericCellValue()));
                        Cell perim = row.getCell(21);
                        storageUnit.setPerim(Float.valueOf((float) perim.getNumericCellValue()));
                        Cell shape = row.getCell(22);
                        storageUnit.setShape(Float.valueOf((float) shape.getNumericCellValue()));

                        Cell frame = row.getCell(23);
                        storageUnit.setFrame((int) frame.getNumericCellValue());
                        Cell loclib = row.getCell(24);
                        storageUnit.setLocLib((int) loclib.getNumericCellValue());

                        mag250.add(storageUnit);



                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    void readNameFile(File file){
        try {
            XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(file));

            for (Sheet sheet : wb ) {
                for (Row row : sheet) {
                    int rowId = row.getRowNum();
                    if(rowId != 0){

                        Cell point = row.getCell(0);
                        Cell partition = row.getCell(1);
                        nameselement.add(new NameData(point.getStringCellValue(), partition.getNumericCellValue()));



                        if(rowId<=6){
                            Cell sizet = row.getCell(2);
                            nameSizeData.add(new NameSizeData((sizet.getNumericCellValue()+ rowId),
                                    sizet.getNumericCellValue()));
                            if(rowId==1){
                                Cell partMaxs = row.getCell(3);

                                setPartMax((int) partMaxs.getNumericCellValue());
                            }


                        }


                    }
                }
            }




        } catch (IOException e) {
            e.printStackTrace();
        }
        for(int i = 1; i< nameSizeData.size(); i++ ){
            nameSizeData.get(i-1).setSizeName(String.valueOf(nameSizeData.get(i-1).getSizeRange() - nameSizeData.get(i).getSizeRange()));

        }
        nameSizeData.get(5).setSizeName( "UNUSED");



    }
}
