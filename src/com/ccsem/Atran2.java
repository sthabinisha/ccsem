package com.ccsem;

import com.ccsem.common.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;


public class Atran2 {
    public static ArrayList<CCSEMStorageUnit> storageUnits = new ArrayList<>();
    int MaxParticles;
    //public static int numMag;
    public static float coalEsce;
    public static float arrayItems = 6001, unitSize, objectSize ;

    public static float[] maxframe = new float[3], field = new float[3], avg = new float[3];
    public static float[][] frag = new float[6][28];
    public static float[] temp = {0, 0, 0};
    private static DecimalFormat df = new DecimalFormat("0.0");

    public static float[] correct = new float[28];
    public static float[] size_range = new float[19];
    public static float[] ttotal= { 0, 0, 0};
    public static float[][] percent = new float[6][28], total = new float[20][28];
    public static ArrayList<MajorElements> majorElementsArray = new ArrayList<>();

    public static int[][] number = new int[6][28];
    public static int arrayIndex = 0, blend = 1;
    public static double siTrans = 0.0, volumeTrans = 0.0, surfTrans = 0.0;
    public static String[] outputName = {"COALESC0.xls", "COALESC1.xls", "COALESC2.xls", "COALESC3.xls", "COALESC4.xls"};


    public static String [] siTransName = {"SITRANS0.PRN", "SITRANS1.PRN", "SITRANS2.PRN", "SITRANS3.PRN", "SITRANS4.PRN"};

    public static double[] specificGravity = {0.00, 2.65, 3.80, 3.61, 4.90, 4.00, 3.30, 3.45, 3.50, 2.23, 2.17, 2.60, 2.80, 2.65, 2.60, 2.65, 2.65, 4.40, 3.09, 2.80, 5.00, 4.60, 5.30, 2.50, 5.72, 3.20, 2.80, 1.99, 3.50, 2.60, 2.65, 2.60, 2.60, 2.70};

    public static File outputFile;


    public static int MAXPARTICLES = 0;
    public static Random randum;
    public static int valueChangeofI= 0;
    public static File comboAshFile;
    public static HashSortedArray hashSortedArray;
    public static MajorElements majorElements;
    public static ArrayList<HashSortedArray> hashDirect = new ArrayList<>();
    public static  ArrayList<Coalescence> coalescenceArrayList = new ArrayList<>();
    public static Coalescence coalescence = new Coalescence();
    public static FileIO fileio;
    public static List<SITransData> siTransDataList;

    //  public  static PointerUnit pivot, temps;

    public static CompoundName compoundName;
    /*Atran2()
     * Description: Coalescence and fragmentation of Included particles
     *
     * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications:Memory setup is removed as for java it is not required
     * Re-Modified by Binisha and copied in Java format
     *
     * Modification  2020- removed the memory model for the large arrays to the global heap which was updated on  1/5/96

     *  Company: Energy & Environmental Research Center
     * */
    public ArrayList<Coalescence> ProcessAtran2(ArrayList<CCSEMStorageUnit> LockedStorageUnits, int MAXPATICLES, int size, float coalEse, float[] sizeNumber) {
        this.coalEsce = coalEse;
        fileio = new FileIO();

        this.storageUnits = LockedStorageUnits;
        randum = new Random();

        randum.setSeed(123456789);
        this.MAXPARTICLES = MAXPATICLES;
        valueChangeofI = size;
        hashSortedArray = new HashSortedArray();
        majorElements = new MajorElements();

        field[0] = 115600;
        field[1] = 2496400;
        size_range = sizeNumber;
        getBinCorrection();

        initialize();
        processMagPhaseTwo( MaxParticles, 0);
        adjustTotal();
        sort(hashDirect,1,  arrayIndex); // noIdea

        adjustShed(outputName[0]);
        siTransDataList = new ArrayList<>();

        siTransDataList.add(new SITransData(siTrans, volumeTrans, surfTrans));
        //writeSITransFile(siTransName[0], df.format(siTrans), df.format(volumeTrans), df.format(surfTrans));


        return coalescenceArrayList;
    }

    /*
     *  Initializes
     *
     * Description: Initializes number, percent, frag, Total too 0
     *Still pointless to assign 0 value.
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications: Used collections.sort() method to sort a simple arraylist.
     * Re-Modified by Binisha and copied in Java format
     *
     * * Company: Energy & Environmental Research Center
     * */
    private static void initialize() {
        int type, size;
        for (int i = 0; i < 2; i++) {
            temp[i] = maxframe[i]  = 0;
        }
        for (int i = 1; i < 6; ++i) {
            for (int j = 1; j < 28; ++j) {
                number[i][j] = 0;
                percent[i][j] = 0;
                frag[i][j] = 0;
                total[i][j] = 0;


            }

        }
    }
    /*
     * Process Magnification phases
     *
     * Description: This function reads the chemical attributs of 2000 particles from inFileName and corrects the diameter of each particle
     * (due to its fragmentation - as estimated by atran1.c). After that it gets the size bin and phase type of the particle
     * and updates the "frag, "Data, pointer and arrayIndex.
     *
     * preCondn
     * InFileName: name of input file
     * Temp: Number of particles. (Temp [0] = # in highest magnification)
     * number: array denoting number of particles of a specified type and size
     * frag: array denoting number of fragmented particles in each size and type
     * i = current blend number o the coal and it is assigned as 1 universally.
     * correction: Array denoting the correction factors in each size
     * arrayIndex: Number of particles n the array
     * MaxFrame: This is the max. frame size used in the data file.
     * sizeRange: Array representing the partitioning of the size bins.
     *
     * postCondn
     * temp, Number, Frag, maxFrame, pointer, arrayIndex and Data parameters are updated with the new particle chemistry data
     * sorts size bins, making smallest size first.
     * type: There are 5 types of particles
     * 1 - Iron fragmentation and iron misses
     * 2 - Kaolinite fragmentation
     * 3- Quartz fragmentation
     * 4 - Dolomite/Calcite fragmentation
     * 5 - None fragmenting particles**
     *
     * Modification: TTotal- correcting the number of particles for fragmentation and x-section frequency
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications: Used collections.sort() method to sort a simple arraylist.
     * Re-Modified by Binisha and copied in Java format
     *
     * * Company: Energy & Environmental Research Center
     * */
    private static void processMagPhaseTwo( int MAXPARTICLES, int I) {

        int partNo = 0;
        int type, size,  count, frame, locLib, exitCondn = 0;
        float oldDiameter;
        float numberParticles;
//        fileIO.tprintf(comboashfileName,"\n\n\n");
//

        for (int i = 0; i < storageUnits.size(); i++) {
//                fileIO.tprintf(comboashfileName, String.format("%1$"+20+ "s", compoundName.Name((int) storageUnits.get(i).getPhase()))+ "  " + String.format("%1$"+5+ "s", storageUnits.get(i).getNA())+ "  " +String.format("%1$"+5+ "s", storageUnits.get(i).getMg())+ "  " + String.format("%1$"+5+ "s", storageUnits.get(i).getAl())+ "  " + String.format("%1$"+5+ "s", storageUnits.get(i).getSi())+ "  " + String.format("%1$"+5+ "s", storageUnits.get(i).getP())+ "  " + String.format("%1$"+5+ "s", storageUnits.get(i).getS())+ "  " + String.format("%1$"+5+ "s", storageUnits.get(i).getCl())+ "  " + String.format("%1$"+5+ "s", storageUnits.get(i).getK())+ "  " + String.format("%1$"+5+ "s", storageUnits.get(i).getCa())+ "  " + String.format("%1$"+5+ "s", storageUnits.get(i).getFe())+ "  " + String.format("%1$"+5+ "s", storageUnits.get(i).getBa())+ "  " + String.format("%1$"+5+ "s", storageUnits.get(i).getTi()));

            if(i>=valueChangeofI){
                I = 1;
            }
            oldDiameter = storageUnits.get(i).getAvgDiameter();
            if(storageUnits.get(i).getFrager() > 1.0){//storage dia is corrected if the frag> 1.5 particles fragmented
                storageUnits.get(i).setAvgDiameter((float) (storageUnits.get(i).getAvgDiameter()/Math.pow(storageUnits.get(i).getFrager(), 0.333)));

                type = getPhasetype(storageUnits.get(i).getPhase());
            }else{
                type = 5;// nonfrag particles
            }

            storageUnits.get(i).setFrager((storageUnits.get(i).getFrager() * storageUnits.get(i).getAvgDiameter()) /(oldDiameter/storageUnits.get(i).getDiaRatio()));

            size = getSizegroup(I, storageUnits.get(i).getAvgDiameter(), size_range);
            //moved from adjust total sept 1994 by SA
            numberParticles = (correct[I * 9 + 9]/storageUnits.get(i).getAvgDiameter()) * storageUnits.get(i).getFrager();

            total[type][size] += numberParticles;
            ttotal[I] += numberParticles;
            // if # of particles in a given size and type range is < 5000 update it.
            if(number[type][size]<5000){
                ++number[type][size];
                frag[type][size] += storageUnits.get(i).getFrager();
                count = number[type][size];
                setArray(type, size, count, arrayIndex, hashDirect, storageUnits.get(i));
            }
            //find the maximum number of frames so far.

            for (int j = 0; j <2 ; j++) {
                if(storageUnits.get(i).getFrame()>maxframe[j]){
                    maxframe[j] = storageUnits.get(i).getFrame();

                }
            }




        }

    }
    /*
     *GetBin Correction
     *
     * Description: The function opens size.prn and reads the size range limits. it also determines the correction
     * for each size bin using the formula: correction[i] = (size[i-1] + size[i])/2. Recall that the size ranges are
     * determined by atran1 after reading the number of magnification and loparticle & hiparticles size for each magnification range.
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 7/27/94
     * Re-Modified by Binisha and copied in Java format
     *
     * * Company: Energy & Environmental Research Center
     * */
    private static void getBinCorrection() {
//        File sizeFile = new File("SIZE.PRN");
//
//        size_range = fileio.readlinebyline();

        correct[0]=1;
        for (int i = 1; i<=2 *9 ; i++) {
            //fileIO.tsanf(sizeFile, sizerange[i]);
            correct[i] = size_range[i-1] + size_range[i]/2;
        }
        correct[19] = size_range[19]/2;
        correct[10] = (0 + size_range[10])/2;
        correct[1] = (0 + size_range[1])/2;
    }


    /*
     * Adjust Shed
     *
     * PreCondn
     * Correction: Array holding the correction factors for each size bin
     * numMag: Denotes the number of magnifications
     * ttotal: Correcting the number of particles for fragmentation and x-section frequency.
     * Field: numMag-1 holds the fieldfor the lowest magnification #
     *
     * postCond
     * NumPart: Total # of particles in each magnification after correcting # of frames and different frame sizes
     * during the ccsem analysis
     * choice: Is a function of the # of particles in each magnification, after correction. Choice is the chance of
     * picking a particle out of 10000000 from low magnification
     *
     * Squish: A function used to coalesce particles.
     * Modifications: SA 1994,
     *
     * numPart: removes using avg dia size for each mag and replaced using the correction sizes
     * Hi and low random #'s: IrandomNo function was skewing data. Replaced with borlands random() function,
     * which is limited by only picking integer numbers, -32, 768, to 32,767. Therefore I need to pick a # from
     * 0 to 25000 and multiple it by 40 or 400 to achieve the necessary values. Testing showed that this mult didn't
     * skew the data. Note: random(num) returns num-1.
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 7/27/94
     * Re-Modified by Binisha and copied in Java format
     *
     * * Company: Energy & Environmental Research Center
     * */
    private static void adjustShed(String outPutFileName)  {

        int index, partNo = 0, particles, last;
        int type, out, q = 0;
        int r = 0, start, rn, size;
        int counter, finish, t, i = 0, condn;
        long pick1 = 0, hiOrLo = 0, pick1total = 0, hiOrLoTotal = 0;
        double[] choice = {10000000.0, 10000000.0};
        float volume, base = 0, area;
        float[] numPart = {0, 0, 0};
        int[][] randomwatch = new int[6][19];
        File outfile;

        for (int j = 0; j < 2; j++) {

            numPart[j] = (ttotal[j] * (correct[18] / correct[9 + 9 * j]) * field[2 - 1] * maxframe[2 - 1]) / (maxframe[j] * field[j]);
            base += numPart[j];
        }
        switch (2) {
            case 1:
                choice[0] = 10000000.0;
                break;
            case 2:
                choice[0] = numPart[0] / base * 10000000;
                break;
            case 3:
                choice[0] = numPart[0] / base * 10000000;
                choice[1] = numPart[1] / base * 10000000 + choice[0];
                break;
            default:
                break;

        }
        //outfile = fileIO.createFile(outPutFileName);
        //createLiberaterFile(outPutFileName);
      //  createHeaderFile(outPutFileName);

        particles = MAXPARTICLES;//numParticles(added total of the particles from the two mag file)
        for (partNo = 1; partNo < particles; partNo++) {
            do {
                do {
                    //last =2;// changed from random value to 10 as it throws an error for Indexoutofbound
                    last = randum.nextInt((int) particles);// changed to particles temporary as the coalese value is greater than size. When particles is greater that coalesce value we need to change
                    //coalEsce instead of particles

                } while ((last < 1));

                for ( counter = 1; counter <= last; ++counter) {
                    hiOrLo = 0;
                    pick1 = 0;



                    do {
                        hiOrLoTotal = randum.nextInt(25000 + 1);
                        //hiOrLoTotal= 10;
                    } while (hiOrLoTotal < 1);
                    hiOrLo = hiOrLoTotal * 400;

                    do {
                        pick1total = randum.nextInt(25000 + 1);

                    } while (pick1total < 1);
                    pick1 = pick1total * 40;
                    if (hiOrLo <= choice[0]) {
                        start = 1;
                        finish = 9;

                    } else if (hiOrLo <= choice[1]) {
                        start = 10;
                        finish = 18;
                    } else {
                        start = 19;
                        finish = 27;

                    }
                    out = condn = 0;
                    for (type = 0; type <= 5; ++type) {
                        for (size = start; size <= finish; ++size) {
                            if (pick1 <= percent[type][size])
                                out = out + 1;
                            if (out == 1) {
                                q = type;
                                r = size;
                                condn = 1;
                            }

                        }

                    }// end counter loop;
                    rn = randum.nextInt(number[q][r] + 1);



                }
            } while ((storageUnits.get(1).getAvgDiameter() <0.01) /*|| number[q][r] == 0*/); // commented temporarily as the loop never ends.
//
            squish(outPutFileName, (int) last, partNo);

        }
    }
    /*
     * Squish
     *
     * Description: Combines the volume of several ash particles together and produces a new particle ( nw volume,
     * composition, radius and density). The number of particles which do coalesce depends upon the random number
     * generator( Function adjust shed). The under should be between 1 and the coalesce #.
     *
     *Modification memory and size setup is removed as it is not required in Java. Binisha
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications: Used collections.sort() method to sort a simple arraylist.
     * Re-Modified by Binisha and copied in Java format
     *
     * * Company: Energy & Environmental Research Center
     * */
    private static void squish(String outPutFileName, int last, int partno) {


        float area, diameter = 0, summer, tempTimesPhase;
        int i;
        float avgDensity = 0, temp;
        float weight = (float) 0.00000001;
        float volume = (float) 0.00000001;
        float totalNa = 0;
        float totalMg = 0;
        float totalAl = 0;
        float totalSi =  0;
        float totalP = 0;
        float totalS = 0;
        float totalCl =  0;
        float totalK =  0;
        float totalCa =  0;
        float totalFe =  0;
        float totalBa =  0;
        float totalTi =  0;
        for (int j = 0; j < last; j++) {
            // int j = partno;

            temp = fileio.volumeSphere(storageUnits.get(j).getAvgDiameter()/2);
            tempTimesPhase = (float) (temp * specificGravity[(int) storageUnits.get(j).getPhase()]);


            if (temp != 0) {
                totalNa += ((storageUnits.get(j).getNA() / 100) * tempTimesPhase);
                totalMg += ((storageUnits.get(j).getMg() / 100) * tempTimesPhase);
                totalAl += ((storageUnits.get(j).getAl() / 100) * tempTimesPhase);
                totalSi += ((storageUnits.get(j).getSi() / 100) * tempTimesPhase);
                totalP += ((storageUnits.get(j).getP() / 100) * tempTimesPhase);
                totalS += ((storageUnits.get(j).getS() / 100) * tempTimesPhase);
                totalCl += ((storageUnits.get(j).getCl() / 100) * tempTimesPhase);
                totalK += ((storageUnits.get(j).getK() / 100) * tempTimesPhase);
                totalCa += ((storageUnits.get(j).getCa() / 100) * tempTimesPhase);
                totalFe += ((storageUnits.get(j).getFe() / 100) * tempTimesPhase);
                totalBa += ((storageUnits.get(j).getBa() / 100) * tempTimesPhase);
                totalTi += ((storageUnits.get(j).getTi() / 100) * tempTimesPhase);
                weight += tempTimesPhase;
                volume += temp;


            }
        }
        if(volume<= 0){
            System.out.println("Error: volume is 0");
        }
        avgDensity = weight / volume;

        if(weight<= 0){
            System.out.println("Error: weight is 0");

        }
        totalNa /= weight;
        totalMg /= weight;
        totalAl /= weight;
        totalSi /= weight;
        totalP /= weight;
        totalS /= weight;
        totalCl /= weight;
        totalK /= weight;
        totalCa /= weight;
        totalFe /= weight;
        totalBa /= weight;
        totalTi /= weight;


        summer = totalNa + totalMg+ totalAl + totalSi + totalS + totalP + totalCl + totalK + totalCa + totalFe + totalBa + totalTi;
        if(summer >= 0.5) {

            diameter = (float) (2 * Math.pow(((3 * volume) / 12.56), 0.33));
            if (diameter <= 100) {
                area = fileio.surfAreaSphere(diameter/2);
                siTrans += (totalSi * avgDensity * volume);
                volumeTrans += volume;
                surfTrans += area;
                 writeCoalEsceFile( partno, totalNa, totalMg, totalAl, totalSi, totalP, totalS, totalCl, totalK, totalCa, totalFe, totalBa, totalTi, diameter, volume, avgDensity, last);
                //++partno;
            }
        }


    }

    private static void writeCoalEsceFile(int partno, float totalNa, float totalMg, float totalAl, float totalSi, float totalP, float totalS, float totalCl, float totalK, float totalCa, float totalFe, float totalBa, float totalTi, float diameter, float volume, float avgDensity, int last) {
        coalescence = new Coalescence();
        coalescence.setPoint(partno);
        coalescence.setCtype(66);
        coalescence.setCts(1000);
        coalescence.setNA((float) (totalNa*100 + 0.5));
        coalescence.setMg((float) (totalMg *100 + 0.5));
        coalescence.setAl((float) (totalAl *100 + 0.5));
        coalescence.setSi((float) (totalSi *100 + 0.5));
        coalescence.setP((float) (totalP *100 + 0.5));
        coalescence.setS((float) (totalS *100 + 0.5));
        coalescence.setCl((float) (totalCl *100 + 0.5));
        coalescence.setK((float) (totalK *100 + 0.5));
        coalescence.setCa((float) (totalCa *100 + 0.5));
        coalescence.setFe((float) (totalFe *100 + 0.5));
        coalescence.setBa((float) (totalBa *100 + 0.5));
        coalescence.setTi((float) (totalTi *100 + 0.5));
        coalescence.setXcoord(1000);
        coalescence.setYcoord(1000);
        coalescence.setAvgdiameter(diameter);
        coalescence.setMaxDiameter(diameter);
        coalescence.setMinDiameter(diameter);
        coalescence.setPv(volume);
        coalescence.setShape(1);
        coalescence.setPerim(2);
        coalescence.setFrame(20);
        coalescence.setAvgDensity(avgDensity);
        coalescence.setCnumb(last);
        coalescenceArrayList.add(coalescence);




    }

    /*
     * Sort Array
     *
     * Description: Set Array, sortArray and getArray are internal memory manipulation routines. THey are written to facilitate better and more structured memory consumption.
     * Basically, all particles are stored in a huge data matrix, which has a field for denoting the hash values for each element to fascilitate fast retrieval.
     * Sort array sorts all elements based on the hash values. Set array stores a new element into the global array whereas GetArray gets a new element from the hash value furnished.
     *
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications: Used collections.sort() method to sort a simple arraylist.
     * Re-Modified by Binisha and copied in Java format
     *
     * * Company: Energy & Environmental Research Center
     * */
    private static void sort(ArrayList<HashSortedArray> hashSort, int left,  int right) {

        Collections.sort(hashSort, HashSortedArray.hashSortedArrayComparator);

    }

    /*
     * Set Array
     *
     * Description: Set Array, sortArray and getArray are internal memory manipulation routines. THey are written to facilitate better and more structured memory consumption.
     * Basically, all particles are stored in a huge data matrix, which has a field for denoting the hash values for each element to fascilitate fast retrieval.
     * Sort array sorts all elements based on the hash values. Set array stores a new element into the global array whereas GetArray gets a new element from the hash value furnished.
     *
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 7/27/94
     * Re-Modified by Binisha and copied in Java format
     *
     * * Company: Energy & Environmental Research Center
     * */
    private static void setArray(int phase, int size, int count, int arrayIndexx, ArrayList<HashSortedArray> hashDirects, CCSEMStorageUnit storageUnits) {


        //  ++arrayIndex;
        majorElements = new MajorElements();
        hashSortedArray = new HashSortedArray();
        hashSortedArray.setHashValue(Float.parseFloat(df.format( phase * 10000001 + size * 10000 + count)));
        hashSortedArray.setIndex(arrayIndex);
        majorElements.setAL(storageUnits.getAl());
        majorElements.setBA(storageUnits.getBa());
        majorElements.setCA(storageUnits.getCa());
        majorElements.setS(storageUnits.getS());
        majorElements.setSI(storageUnits.getSi());
        majorElements.setP(storageUnits.getP());
        majorElements.setK(storageUnits.getK());
        majorElements.setFE(storageUnits.getFe());
        majorElements.setMG(storageUnits.getMg());
        majorElements.setNA(storageUnits.getNA());
        majorElements.setCL(storageUnits.getCl());
        majorElements.setTI(storageUnits.getTi());
        majorElements.setDiameter(storageUnits.getAvgDiameter());

        majorElementsArray.add(majorElements);


        hashDirect.add(hashSortedArray);


    }//SetArray

    /*
     *  Adjust total
     *
     * Description:This updates the percent array. Adds cumulative chance of picking a particle from the bin - one
     * for low magnification and an other for high magnification.
     *
     * precondn
     * Frag: Denotes the array holding the fragmented particles
     * correction: array holding the correction factors for each size bin
     * NumMag: Denotes the number of magnifications.
     * TTotal: Correcting the number of particles for fragmentation and x-section frequency.
     *
     * postCondn
     * percent: An double array containing a matrix of 5 particle types and 18 size bins.
     * The number represents the cumulative chance of picking a particle out of that particular bin.
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications: Used collections.sort() method to sort a simple arraylist.
     * Re-Modified by Binisha and copied in Java format
     *
     * * Company: Energy & Environmental Research Center
     * */
    private static void adjustTotal() {
        float[] newTotal = new float[3], addOn = new float[3];
        int size, type;
        for (int i = 0; i < 2; i++) {
            newTotal[i] = ttotal[i] / 1000000;
            addOn[i] = 0;
        }
        for (int i = 0; i < 2; i++) {

            for (int j = 1; j < 5; ++j) {
                for (int k = 1 ; k <=9; ++k) {
                    percent[j][i* 9 + k] = total [j][i * 9 + k]/ newTotal[i]+ addOn[i];
                    addOn[i] = percent[j][k + i * 9];

                }
            }
        }
    }//adjustTotal


    private static int getSizegroup(int i, float diameter, float[] sizerange) {
        /*Size is based on the serial number of the size printed on SIZE.PRN and the diameter*/
        for (int j = 0; j <18 ; j++) {
            if(diameter<=sizerange[j]){
                return j;
            }

        }
        return 0;
    }

    private static int getPhasetype(float phase) {
        if(phase == 4){
            return 1;
        }else
            return 3;
    }
    private static void writeSITransFile(String s, String siTras, String volumeTras, String surfTras) {
        try {
            File file = fileio.createFile(s);
            if(file.length()!= 0){
                file.delete();
                file = fileio.createFile(s);

            }

            FileWriter myWriter = new FileWriter(file, true);

            myWriter.write(siTrans + " "+ volumeTrans + " "+ surfTrans );
            myWriter.write("\n");
            myWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
