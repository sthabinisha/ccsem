package com.ccsem;

import com.ccsem.common.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import static com.ccsem.Atran2.siTransDataList;
import static com.ccsem.Atran5.fileio;


public class Atran3 {
    private static int numMag = 2;


    private static float[] maxframe = new float[3], field = new float[3], avg = new float[3];
    private static float[][] frag = new float[6][28];
    private static float[] temp = {0, 0, 0};
    private static DecimalFormat df = new DecimalFormat("0.0");

    private static float[] correction = new float[28];
    private static float[] sizerange = new float[19];
    private static float[] ttotal= { 0, 0, 0};
    private static float[][] percent = new float[6][28], total = new float[20][28];
    private static ArrayList<MajorElements> majorElementsArray = new ArrayList<>();

    private static int[][] number = new int[6][28];
    private static int arrayIndex = 0;
    private static double siTrans = 0.0, volumeTrans = 0.0, surfTrans = 0.0;
    private static double[] density = {0.00, 2.65, 3.80, 3.61, 4.90, 4.00, 3.30, 3.45, 3.50, 2.23, 2.17, 2.60, 2.80, 2.65, 2.60, 2.65, 2.65, 4.40, 3.09, 2.80, 5.00, 4.60, 5.30, 2.50, 5.72, 3.20, 2.80, 1.99, 3.50, 2.60, 2.65, 2.60, 2.60, 2.70};


    private static int MAXPARTICLES = 0;
    private static Random randum;
    private static int valueChangeofI= 0;
    private static HashSortedArray hashSortedArray;
    private static MajorElements majorElements;
    private static ArrayList<HashSortedArray> hashDirect = new ArrayList<>();
    private static  ArrayList<Coalescence> coalescenceArrayList = new ArrayList<>();
    private static Coalescence coalescence = new Coalescence();
    private static ArrayList<CCSEMStorageUnit> storageUnits = new ArrayList<>();



    //  public  static PointerUnit pivot, temps;

    public static CompoundName compoundName;
    /*Atran3()
     * Description: Fragmentation of Excluded particles
     *
     * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications:Memory setup is removed as for java it is not required
     * Re-Modified by Binisha and copied in Java format
     *
     * Modification  2020- removed the memory model for the large arrays to the global heap which was updated on  1/5/96

     *  Company: Energy & Environmental Research Center
     * */
    public ArrayList<Coalescence> ProcessAtran3(ArrayList<CCSEMStorageUnit> LockedStorageUnits, int MAXPATICLES, int size, float[] sizeNumber) {
        storageUnits = LockedStorageUnits;
        randum = new Random();

        randum.setSeed(123456789);
        MAXPARTICLES= MAXPATICLES;
        hashSortedArray = new HashSortedArray();
        majorElements = new MajorElements();
        field[0] = 115600;
        field[1] = 2496400;
        sizerange = sizeNumber;
        getBinCorrection();


        initialize();
        valueChangeofI = size;
        processMagPhaseTwo(  0);
        adjustTotal();
        sort(hashDirect); // noIdea
        adjustShed();

        siTransDataList.add(new SITransData(siTrans, volumeTrans, surfTrans));


        return coalescenceArrayList;
    }

    /*
     *  Initializes
     *
     * Description: Initializes number, percent, frag, Total too 0
     *Still pointless to assign 0 value.
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications: Used collections.sort() method to sort a simple arraylist.
     * Re-Modified by Binisha and copied in Java format
     *
     * * Company: Energy & Environmental Research Center
     * */
    private static void initialize() {
        for (int i = 0; i < 2; i++) {
            temp[i] = maxframe[i]  = 0;
        }
        for (int i = 1; i < 6; ++i) {
            for (int j = 1; j < 28; ++j) {
                number[i][j] = 0;
                percent[i][j] = 0;
                frag[i][j] = 0;
                total[i][j] = 0;


            }

        }
    }
    private static void processMagPhaseTwo(  int I) {

        int partNo = 0, type, size,  count;
        float oldDiameter, numberParticles;
//

        for (int i = 0; i < storageUnits.size(); i++) {

            if(i>=valueChangeofI){
                I = 1;
            }
            oldDiameter = storageUnits.get(i).getAvgDiameter();
            if(storageUnits.get(i).getFrager() > 1.0){//storage dia is corrected if the frag> 1.5 particles fragmented
                storageUnits.get(i).setAvgDiameter((float) (storageUnits.get(i).getAvgDiameter()/Math.pow(storageUnits.get(i).getFrager(), 0.333)));
                type = getPhasetype(storageUnits.get(i).getPhase());
            }else{
                type = 5;// nonfrag particles
            }
            storageUnits.get(i).setFrager((storageUnits.get(i).getFrager() * storageUnits.get(i).getAvgDiameter()) /(oldDiameter/storageUnits.get(i).getDiaRatio()));
            size = getSizegroup(I, storageUnits.get(i).getAvgDiameter(), sizerange);
            //moved from adjust total sept 1994 by SA
            numberParticles = (correction[I * 9 + 9]/storageUnits.get(i).getAvgDiameter()) * storageUnits.get(i).getFrager();
            total[type][size] += numberParticles;
            ttotal[I] += numberParticles;
            // if # of particles in a given size and type range is < 5000 update it.
            if(number[type][size]<5000){
                ++number[type][size];
                frag[type][size] += storageUnits.get(i).getFrager();
                count = number[type][size];
                setArray(type, size, count, arrayIndex, hashDirect, storageUnits.get(i));
            }
            //find the maximum number of frames so far.

            for (int j = 0; j <2 ; j++) {
                if(storageUnits.get(i).getFrame()>maxframe[j]){
                    maxframe[j] = storageUnits.get(i).getFrame();

                }
            }




        }
        if(temp[I]>0){
            avg[I] /=temp[I];

        }
        else{
            avg[I] = 0;
        }

    }
    /*
     *GetBin Correction
     *
     * Description: The function opens size.prn and reads the size range limits. it also determines the correction
     * for each size bin using the formula: correction[i] = (size[i-1] + size[i])/2. Recall that the size ranges are
     * determined by atran1 after reading the number of magnification and loparticle & hiparticles size for each magnification range.
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 7/27/94
     * Re-Modified by Binisha and copied in Java format
     *
     * * Company: Energy & Environmental Research Center
     * */
    private static void getBinCorrection() {

        correction[0]=1;
        for (int i = 1; i<=2*9 ; i++) {
            correction[i] = sizerange[i-1] + sizerange[i]/2;
        }
        correction[19] = sizerange[19]/2;
        correction[10] = (0 + sizerange[10])/2;
        correction[1] = (0 + sizerange[1])/2;
    }


    /*
     * Adjust Shed
     *
     * PreCondn
     * Correction: Array holding the correction factors for each size bin
     * numMag: Denotes the number of magnifications
     * ttotal: Correcting the number of particles for fragmentation and x-section frequency.
     * Field: numMag-1 holds the fieldfor the lowest magnification #
     *
     * postCond
     * NumPart: Total # of particles in each magnification after correcting # of frames and different frame sizes
     * during the ccsem analysis
     * choice: Is a function of the # of particles in each magnification, after correction. Choice is the chance of
     * picking a particle out of 10000000 from low magnification
     *
     * Squish: A function used to coalesce particles.
     * Modifications: SA 1994,
     *
     * numPart: removes using avg dia size for each mag and replaced using the correction sizes
     * Hi and low random #'s: IrandomNo function was skewing data. Replaced with borlands random() function,
     * which is limited by only picking integer numbers, -32, 768, to 32,767. Therefore I need to pick a # from
     * 0 to 25000 and multiple it by 40 or 400 to achieve the necessary values. Testing showed that this mult didn't
     * skew the data. Note: random(num) returns num-1.
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 7/27/94
     * Re-Modified by Binisha and copied in Java format
     *
     * * Company: Energy & Environmental Research Center
     * */
    private static void adjustShed( ) {
        int index, partNo = 0, particles, last;
        int type, out, q = 0;
        int r = 0, start, rn, size;
        int counter, finish, t, i = 0, condn;
        long pick1 = 0, hiOrLo = 0, pick1total = 0, hiOrLoTotal = 0;
        double[] choice = {10000000.0, 10000000.0};
        float volume, base = 0, area;
        float[] numPart = {0, 0, 0};
        int[][] randomwatch = new int[6][19];
        File outfile;

        for (int j = 0; j < numMag; j++) {
            numPart[j] = (ttotal[j] * (correction[18] / correction[9 + 9 * j]) * field[numMag - 1] * maxframe[numMag - 1]) / (maxframe[j] * field[j]);
            base += numPart[j];
        }
        switch (numMag) {
            case 1:
                choice[0] = 10000000.0;
                break;
            case 2:
                choice[0] = numPart[0] / base * 10000000;
                break;
            case 3:
                choice[0] = numPart[0] / base * 10000000;
                choice[1] = numPart[1] / base * 10000000 + choice[0];
                break;
            default:
                break;

        }
        //outfile = fileIO.createFile(outPutFileName);
        //createLiberaterFile(outPutFileName);
        //  createHeaderFile(outPutFileName);

        particles = MAXPARTICLES;//numParticles(added total of the particles from the two mag file)
        for (partNo = 1; partNo < particles; partNo++) {
           // System.out.println(partNo+ "partNo");
            i = partNo;

            do {
                hiOrLo = 0;
                pick1 = 0;
                do {


                    hiOrLoTotal =  randum.nextInt(25000 + 1);
                    //hiOrLoTotal = 10;
                } while (hiOrLoTotal < 1);
                hiOrLo = hiOrLoTotal * 400;

                do {
                    pick1total =  randum.nextInt(25000 + 1);
                    //pick1total = 10;

                } while (pick1total < 1);
                pick1 = pick1total * 40;
                if (hiOrLo <= choice[0]) {
                    start = 1;
                    finish = 9;

                } else if (hiOrLo <= choice[1]) {
                    start = 10;
                    finish = 18;
                } else {
                    start = 19;
                    finish = 27;

                }
                out = condn = 0;
                for (type = 0; type <= 5; ++type) {
                    for (size = start; size <= finish; ++size) {
                        if (pick1 <= percent[type][size])
                            out = out + 1;
                        if (out == 1) {
                            q = type;
                            r = size;
                            condn = 1;
                        }

                    }

                }
                rn =  randum.nextInt(number[q][r] + 1);
                t = getArray(q, r, rn, arrayIndex);


            }while (storageUnits.get(i).getAvgDiameter()<0.01 ||number[q][r] == 0);
//
            ++randomwatch[q][r];
            if(storageUnits.get(i).getAvgDiameter()<= 100){
                volume = fileio.volumeSphere(storageUnits.get(i).getAvgDiameter()/2);
                writeCoalEsceFile(partNo, storageUnits.get(i).getNA(),storageUnits.get(i).getMg(), storageUnits.get(i).getAl(), storageUnits.get(i).getSi(), storageUnits.get(i).getP(), storageUnits.get(i).getS(), storageUnits.get(i).getCl(), storageUnits.get(i).getK(), storageUnits.get(i).getCa(), storageUnits.get(i).getFe(), storageUnits.get(i).getBa(), storageUnits.get(i).getTi(), storageUnits.get(i).getAvgDiameter(), volume, (float) density[(int) storageUnits.get(i).getPhase()]);
                siTrans += ((storageUnits.get(i).getSi() / 100) * (density[(int) storageUnits.get(i).getPhase()] )* volume);
                volumeTrans += volume;
                area = fileio.surfAreaSphere(storageUnits.get(i).getAvgDiameter()/2);
                surfTrans+= area;

                //++partNo;

            }
        }
    }



    /*
     * Get Array
     *
     * Description: Set Array, sortArray and getArray are internal memory manipulation routines. THey are written to facilitate better and more structured memory consumption.
     * Basically, all particles are stored in a huge data matrix, which has a field for denoting the hash values for each element to fascilitate fast retrieval.
     * Sort array sorts all elements based on the hash values. Set array stores a new element into the global array whereas GetArray gets a new element from the hash value furnished.
     *
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications:Memory setup is removed as for java it is not required
     * Re-Modified by Binisha and copied in Java format
     *
     * * Company: Energy & Environmental Research Center
     * */
    private static int getArray(int q, int r, int rn, int arrayIndex) {
        int hashToFind_U, phase_U = 0, size_U = 0;
        int low, high = 0, mid, count = 0;

        q= phase_U;
        r = size_U;
        hashToFind_U = phase_U * 10_000_001 + size_U * 10_000 + count;
        low = 1;
        high = arrayIndex;

        while(low <= high) {
            mid = (low + high) / 2;
//
            if(hashToFind_U <2){
                high = mid-1;

            }
            else  if (hashToFind_U > 2){
                low =mid + 1;

            }else
                return 2;

        }
        return 0;
    }

        private static void writeCoalEsceFile(int partno, float totalNa, float totalMg, float totalAl, float totalSi, float totalP, float totalS, float totalCl, float totalK, float totalCa, float totalFe, float totalBa, float totalTi, float diameter, float volume, float avgDensity) {
        coalescence = new Coalescence();
        coalescence.setPoint(partno);
        coalescence.setCtype(66);
        coalescence.setCts(1000);
        coalescence.setNA((float) (totalNa));
        coalescence.setMg((float) (totalMg ));
        coalescence.setAl((float) (totalAl ));
        coalescence.setSi((float) (totalSi ));
        coalescence.setP((float) (totalP ));
        coalescence.setS((float) (totalS ));
        coalescence.setCl((float) (totalCl ));
        coalescence.setK((float) (totalK ));
        coalescence.setCa((float) (totalCa ));
        coalescence.setFe((float) (totalFe ));
        coalescence.setBa((float) (totalBa));
        coalescence.setTi((float) (totalTi ));
        coalescence.setXcoord(1000);
        coalescence.setYcoord(1000);
        coalescence.setAvgdiameter(diameter);
        coalescence.setMaxDiameter(diameter);
        coalescence.setMinDiameter(diameter);
        coalescence.setPv(volume);
        coalescence.setShape(1);
        coalescence.setPerim(1);
        coalescence.setFrame(1);
        coalescence.setAvgDensity(avgDensity);
        coalescenceArrayList.add(coalescence);




    }

    /*
     * Sort Array
     *
     * Description: Set Array, sortArray and getArray are internal memory manipulation routines. THey are written to facilitate better and more structured memory consumption.
     * Basically, all particles are stored in a huge data matrix, which has a field for denoting the hash values for each element to fascilitate fast retrieval.
     * Sort array sorts all elements based on the hash values. Set array stores a new element into the global array whereas GetArray gets a new element from the hash value furnished.
     *
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications: Used collections.sort() method to sort a simple arraylist.
     * Re-Modified by Binisha and copied in Java format
     *
     * * Company: Energy & Environmental Research Center
     * */
    private static void sort(ArrayList<HashSortedArray> hashSort) {

        Collections.sort(hashSort, HashSortedArray.hashSortedArrayComparator);

    }

    /*
     * Set Array
     *
     * Description: Set Array, sortArray and getArray are internal memory manipulation routines. THey are written to facilitate better and more structured memory consumption.
     * Basically, all particles are stored in a huge data matrix, which has a field for denoting the hash values for each element to fascilitate fast retrieval.
     * Sort array sorts all elements based on the hash values. Set array stores a new element into the global array whereas GetArray gets a new element from the hash value furnished.
     *
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 7/27/94
     * Re-Modified by Binisha and copied in Java format
     *
     * * Company: Energy & Environmental Research Center
     * */
    private static void setArray(int phase, int size, int count, int arrayIndexx, ArrayList<HashSortedArray> hashDirects, CCSEMStorageUnit storageUnits) {


        //  ++arrayIndex;
        majorElements = new MajorElements();
        hashSortedArray = new HashSortedArray();
        hashSortedArray.setHashValue(Float.parseFloat(df.format( phase * 10000001 + size * 10000 + count)));
        hashSortedArray.setIndex(arrayIndex);
        majorElements.setAL(storageUnits.getAl());
        majorElements.setBA(storageUnits.getBa());
        majorElements.setCA(storageUnits.getCa());
        majorElements.setS(storageUnits.getS());
        majorElements.setSI(storageUnits.getSi());
        majorElements.setP(storageUnits.getP());
        majorElements.setK(storageUnits.getK());
        majorElements.setFE(storageUnits.getFe());
        majorElements.setMG(storageUnits.getMg());
        majorElements.setNA(storageUnits.getNA());
        majorElements.setCL(storageUnits.getCl());
        majorElements.setTI(storageUnits.getTi());
        majorElements.setDiameter(storageUnits.getAvgDiameter());

        majorElementsArray.add(majorElements);


        hashDirect.add(hashSortedArray);


    }//SetArray

    /*
     *  Adjust total
     *
     * Description:This updates the percent array. Adds cumulative chance of picking a particle from the bin - one
     * for low magnification and an other for high magnification.
     *
     * precondn
     * Frag: Denotes the array holding the fragmented particles
     * correction: array holding the correction factors for each size bin
     * NumMag: Denotes the number of magnifications.
     * TTotal: Correcting the number of particles for fragmentation and x-section frequency.
     *
     * postCondn
     * percent: An double array containing a matrix of 5 particle types and 18 size bins.
     * The number represents the cumulative chance of picking a particle out of that particular bin.
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications: Used collections.sort() method to sort a simple arraylist.
     * Re-Modified by Binisha and copied in Java format
     *
     * * Company: Energy & Environmental Research Center
     * */
    private static void adjustTotal() {
        float[] newTotal = new float[3], addOn = new float[3];
        int size, type;
        for (int i = 0; i < numMag; i++) {
            newTotal[i] = ttotal[i] / 1000000;
            addOn[i] = 0;
        }
        for (int i = 0; i < numMag; i++) {

            for (int j = 1; j < 5; ++j) {
                for (int k = 1 ; k <=9; ++k) {
                    percent[j][i* 9 + k] = total [j][i * 9 + k]/ newTotal[i]+ addOn[i];
                    addOn[i] = percent[j][k + i * 9];

                }
            }
        }
    }//adjustTotal


    private static int getSizegroup(int i, float diameter, float[] sizerange) {
        /*Size is based on the serial number of the size printed on SIZE.PRN and the diameter*/
        for (int j = 0; j <18 ; j++) {
            if(diameter<=sizerange[j]){
                return j;
            }

        }
        return 0;
    }

    private static int getPhasetype(float phase) {
        if(phase == 4){
            return 1;
        }else
            return 3;
    }

}
