package com.ccsem;

import com.ccsem.common.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import static com.ccsem.App.getPhase;
/*
Atran5.cpp

Description: This program is designed to resolve a mass balance on a coal system from CCSEM and XRF
inputs:
XRF File: oxides-> Si, Al, Fe, Ti, P, Ca, Mg, Na, K
CCSEM File: Point, Na, mg, al, Si, P, S, Cl, k, Ca, Fe, ba, Ti,
AvgDiameter, Area, Frame, Loclib (1= locked, 2= liberated))

Outputs: Chemout0.prn: Three column headings XRF, CCSEM, Organics Associated Oxides order -> Na,
mg, al, si, p, k, ca, fe, ti

* Binish update: removed the method that assign just the zeron value
* Author: Original by Tom Erickson, c version written by Shaker.
* Modifications and testing done by Sean Allan 1994
* Re-Modified by Binisha and copied in Java format
* Company: Energy & Environmental Research Center

* */

public class Atran5 {
    List<CCSEMStorageUnit> magnification1 = new ArrayList<>();
    List<CCSEMStorageUnit> magnification2 = new ArrayList<>();
    public static List<ChemOutData> chemOutData;
    public static ArrayList<AllElementsOxide> allElementsOxides = new ArrayList<>();
    public static AllElementsCCS allElementsCCS;
    private static AllElementsOxide allElementsOxide;
    public static int i, k, blend = 1, type, numMag ;

    public static float phase[] = new float[34];
    public static float totalphase;
    public static float[] total=  new float[34];
    public static String ultimate , xray;

    public static float[] XRF = new float[10];
    private static DecimalFormat df = new DecimalFormat("0.000");





    public static AllElementsOrg allElementsORG = new AllElementsOrg();
    public static float NA, MG, SI, P, S, CL, K, CA, FE, BA, TI, AL;

    public static FileIO fileio;
    public static float low[] = {1, 10, 100};
    public static float high[]= {10, 50, 1000};
    public static float weight;
    public static float[] correction = new float[3];




    /*
     * Atran5.cpp
     *
     * Description: This program is designed to resolve a mass balance on a coal system from ccsem and xrf
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 1994
     * Re-Modified by Binisha and copied in Java format
     * Company: Energy & Environmental Research Center
     * */

    public void processAtran5(MagnificationServer magnificationServer, float[] corection) {

        chemOutData = new ArrayList<>();
        getHiLo(low, high); // It is the hardcoded data

        allElementsOxide = new AllElementsOxide();
        allElementsCCS= new AllElementsCCS();
        chemOutData = new ArrayList<>();

        this.correction = corection;
        getPhase = new GetPhase();

        initPhoxCo();
        fileio = new FileIO();
        magnification1 = processCCSEMFILE(magnificationServer.getMag50() ,correction[0], low[0], high[0]);
        magnification2 = processCCSEMFILE(magnificationServer.getMag250(), correction[1], low[1], high[1]);

        addTotal(  phase);
        adjustOx34( );
        //Normalizing to exclude sulphur and barium
        adjustCCS(allElementsCCS, allElementsOxides.get(34));
        readXrayFile("XRAY.PRN" );
        //renormalizing to sio2 with xrf data
        reNormalizeXRF();

        findOrg();

        adjustBalOrgCcs();

        writeChemFile(XRF, allElementsCCS, allElementsORG);


        //return chem;
    }



    private List<CCSEMStorageUnit> processCCSEMFILE(List<CCSEMStorageUnit> magnificatio, float correction, float low, float high) {
        List<CCSEMStorageUnit> magnification = new ArrayList<>();
        magnification =  magnificatio;
        for (int i = 0; i < magnification.size(); i++) {
            if((magnification.get(i).getAvgDiameter() >= low) &&(magnification.get(i).getAvgDiameter() <= high)) {
                //to ensure that there is no division by zero, we make sure al and s are minutely greater than
                //zero as they are both used in ratios for phas determination
                if(magnification.get(i).getAl() == 0.0) magnification.get(i).setAl( (float) 0.0000001);
                if(magnification.get(i).getS() == 0.0)  magnification.get(i).setS( (float) 0.0000001);
                magnification.get(i).setAreaCorrected(  magnification.get(i).getArea() * correction);


                type = getPhase.getPhase(magnification.get(i).getAl(), magnification.get(i).getBa(), magnification.get(i).getCa(), magnification.get(i).getCl(), magnification.get(i).getFe(), magnification.get(i).getK(), magnification.get(i).getMg(), magnification.get(i).getNA(), magnification.get(i).getP(), magnification.get(i).getS(), magnification.get(i).getSi(), magnification.get(i).getTi());

                weight = calcWeight(magnification.get(i).getAreaCorrected(), type, magnification.get(i).getCa(), magnification.get(i).getMg(), magnification.get(i).getFe());
                calcOxides(allElementsOxides.get(type), type, weight, magnification.get(i).getAl(), magnification.get(i).getBa(), magnification.get(i).getCa(), magnification.get(i).getCl(), magnification.get(i).getFe(), magnification.get(i).getK(), magnification.get(i).getMg(), magnification.get(i).getNA(), magnification.get(i).getP(), magnification.get(i).getS(), magnification.get(i).getSi(), magnification.get(i).getTi());

            }
        }
        return magnification;

    }


    /*
     * Add Total
     *
     * Description: Initializing variables and calculating ratios
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 7/27/94
     * Re-Modified by Binisha and copied in Java format
     *
     * * Company: Energy & Environmental Research Center
     * */
    private static void addTotal( float[] phases) {
        // TODO Auto-generated method stub
        int[] ptr = {2, 6, 7, 8, 22, 23, 24, 25, 26, 31};
        totalphase = phase[1];
        for (int i = 0; i <= 33; i++) {
            totalphase += phase[i];
        }
        for (int i = 0; i < 10; i++) {
            total[ptr[i]] = findTotal(allElementsOxides.get(ptr[i]));

        }
        for (int i = 0; i < 10; i++) {
            total[ptr[i]] += 0.01;
        }
    }


    private static void readXrayFile(String xray) {

        try
        {
            File file=new File(xray);    //creates a new file instance
            FileReader fr=new FileReader(file);   //reads the file
            BufferedReader br=new BufferedReader(fr);  //creates a buffering character input stream
            StringBuffer sb=new StringBuffer();    //constructs a string buffer with no characters
            String line;
            int i = 0;
            while((line=br.readLine())!=null)
            {
                XRF[i]= Float.valueOf(line);
                i++;
            }
            fr.close();    //closes the stream and release the resources
//		System.out.println("Contents of File: ");
//		System.out.println(sb.toString());   //returns a string that textually represents the object

        }
        catch(IOException e)
        {
            e.printStackTrace();
        }

    }
    /*
     * Renormalization
     *
     * Description: Renormalizes CCSEM data, based on the XRF and CCSEM Si totals
     * Missing information for the orgsivalue
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 7/18/94
     * Re-Modified by Binisha and copied in Java format
     *
     * * Company: Energy & Environmental Research Center
     * */
    private static void reNormalizeXRF() {
        float norm1;
        //float orgsivalue = (allElementsCCS.get(0).getSI()/ xrf[1]); // Never mentioned the value
        float orgsivalue= 1;// asigned 1 temporarily
        norm1 = XRF[1] * orgsivalue / allElementsCCS.getSI();
        allElementsCCS.setSI(XRF[1] * orgsivalue);
        allElementsCCS.setAL(allElementsCCS.getAL() * norm1);
        allElementsCCS.setNA(allElementsCCS.getNA() * norm1);
        allElementsCCS.setP(allElementsCCS.getP() * norm1);
        allElementsCCS.setMG(allElementsCCS.getMG() * norm1);
        allElementsCCS.setK(allElementsCCS.getK() * norm1);
        allElementsCCS.setCA(allElementsCCS.getCA() * norm1);
        allElementsCCS.setFE(allElementsCCS.getFE() * norm1);
        allElementsCCS.setTI(allElementsCCS.getTI() * norm1);


    }
    /*
     * Find Organics
     *
     * Description: Determines the amound of organical associated minerals by subtractind the ccs
     * value from the XRF value
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 7/27/94
     * Re-Modified by Binisha and copied in Java format
     *
     * * Company: Energy & Environmental Research Center
     * */
    private static void findOrg() {

        allElementsORG.setNA(XRF[7] - allElementsCCS.getNA());
        allElementsORG.setMG(XRF[6] - allElementsCCS.getMG());
        allElementsORG.setAL(XRF[1] - allElementsCCS.getAL());
        allElementsORG.setSI(XRF[0] - allElementsCCS.getSI());
        allElementsORG.setP(allElementsCCS.getP() - XRF[4]);// changing to get positive value
        allElementsORG.setK(XRF[8] - allElementsCCS.getK());
        allElementsORG.setCA(XRF[5] - allElementsCCS.getCA());
        allElementsORG.setFE(XRF[2] - allElementsCCS.getFE());
        allElementsORG.setTI( allElementsCCS.getTI() -XRF[3]);// changing to get positive value
        // System.out.println(allElementsORG.getP()+ " p"+ allElementsCCS.getP()+ " "+XRF[4] );
        //System.out.println(allElementsORG.getTI()+ " ti"+ allElementsCCS.getTI()+ " "+ XRF[3]);


    }
    /*
     * Adjust ccs
     *
     * Description:Recalculate ccs according to oxide
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 7/27/94
     * Re-Modified by Binisha and copied in Java format
     *
     * * Company: Energy & Environmental Research Center
     * */
    private static void adjustCCS(AllElementsCCS allElementsCCSr, AllElementsOxide allElementsOxide) {
        float ccssum;
        ccssum = findTotal(allElementsOxide);
        allElementsCCS.setSI(  allElementsOxide.getSI() / ccssum);
        allElementsCCS.setAL(  allElementsOxide.getAL() / ccssum);
        allElementsCCS.setNA(  allElementsOxide.getNA() / ccssum);
        allElementsCCS.setP(  allElementsOxide.getP() / ccssum);
        allElementsCCS.setK(  allElementsOxide.getK() / ccssum);
        allElementsCCS.setCA(  allElementsOxide.getCA() / ccssum);
        allElementsCCS.setFE(  allElementsOxide.getFE() / ccssum);
        allElementsCCS.setTI(  allElementsOxide.getTI() / ccssum);


    }
    /*
     * Get Hi lo
     *
     * Description: "Mag.Prn" contains the following
     * 1. Number of magnification that will be used.
     * 2. Magnification
     * Magnif record holds all the magnification values and their guard regions
     * Hip holds the max. particle cutoff size for that magnification
     * Lop holds the min. particle cutoff size for that magnification
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 7/27/94
     * Re-Modified by Binisha and copied in Java format
     *Modification: 1/23/96 SEA hard coded in the Mag.prn file, Atran only handles 2 magnification
     * files, therefore no need to load mag.prn
     * * Company: Energy & Environmental Research Center
     * */
    private static void getHiLo(float[] low2, float[] high2) {
        // TODO Auto-generated method stub
        //240 mag
        high2[0] = 10;
        low2[0] = 1;
        //50.mag
        high2[1] = 50; // usually 100um but trying to stop large CA particles from washing out the small particles
        low2[1] = 10;

    }

    /*
     * Add Oxygen
     *
     * Description: Just adds the 0 value
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 7/27/94
     * Re-Modified by Binisha and copied in Java format
     *
     * * Company: Energy & Environmental Research Center
     * */
    private static void adjustOx34( ) {
        // TODO Auto-generated method stub
        // zeroElements(ccs);
        zeroElementsCCS(allElementsCCS);
        for (int i = 0; i <= 33; i++) {
            allElementsOxides.get(34).setSI(allElementsOxides.get(34).getSI() + allElementsOxides.get(i).getSI()) ;
            allElementsOxides.get(34).setAL(allElementsOxides.get(34).getAL() +  allElementsOxides.get(i).getAL()) ;
            allElementsOxides.get(34).setNA(allElementsOxides.get(34).getNA() +  allElementsOxides.get(i).getNA()) ;
            allElementsOxides.get(34).setMG(allElementsOxides.get(34).getMG() +  allElementsOxides.get(i).getMG()) ;
            allElementsOxides.get(34).setP(allElementsOxides.get(34).getP() +  allElementsOxides.get(i).getP()) ;
            allElementsOxides.get(34).setK(allElementsOxides.get(34).getK() +  allElementsOxides.get(i).getK()) ;
            allElementsOxides.get(34).setFE(allElementsOxides.get(34).getFE() +  allElementsOxides.get(i).getFE()) ;
            allElementsOxides.get(34).setCA(allElementsOxides.get(34).getCA() +  allElementsOxides.get(i).getCA()) ;
            allElementsOxides.get(34).setTI(allElementsOxides.get(34).getTI() +  allElementsOxides.get(i).getTI()) ;
            allElementsOxides.get(34).setBA(allElementsOxides.get(34).getBA() +  allElementsOxides.get(i).getBA()) ;
            allElementsOxides.get(34).setS(allElementsOxides.get(34).getS() +  allElementsOxides.get(i).getS()) ;
            allElementsOxides.get(34).setCL(allElementsOxides.get(34).getCL() +  allElementsOxides.get(i).getCL()) ;


        }
    }


    private static void zeroElementsCCS(AllElementsCCS elements) {
        elements.setSI((float)0.0);
        elements.setAL((float)0.0);
        elements.setNA((float)0.0);
        elements.setMG((float)0.0);
        elements.setP((float)0.0);
        elements.setK((float)0.0);
        elements.setFE((float)0.0);
        elements.setCA((float)0.0);
        elements.setTI((float)0.0);
        elements.setBA((float)0.0);
        elements.setS((float)0.0);
        elements.setCL((float)0.0);
        // TODO Auto-generated method stub

    }

    /*
     * Initial Phase OxCo
     *
     * Description: Opens Field.PRN and loads the size bin corrections and phase oxidation values
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 1994
     * Re-Modified by Binisha and copied in Java format
     *
     * * Company: Energy & Environmental Research Center
     * */
    private static void initPhoxCo() {
        // TODO Auto-generated method stub
        File file;


        for (int i = 0; i < 34; ++i) {
            phase[i] = (float) 0.0; //no
        }

        for (int i = 0; i < 35; i++) {
            zeroElements(allElementsOxide);

        }

    }

    /*
     * Find Total
     *
     * Description: totals the value of the elements
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 1994
     * Re-Modified by Binisha and copied in Java format
     * * Company: Energy & Environmental Research Center
     * */
    private static float findTotal (AllElementsOxide oxide2) {

        float totals = oxide2.getSI()+ oxide2.getAL()+ oxide2.getNA()+ oxide2.getMG()+ oxide2.getP()+ oxide2.getK()+ oxide2.getCA()+ oxide2.getFE()+ oxide2.getTI();

        return totals;
    }//findTotal
    /*
     * Calculate Oxides
     *
     * Description: converts elements to oxides
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 7/27/94
     * Re-Modified by Binisha and copied in Java format
     *
     * * Company: Energy & Environmental Research Center
     * */
    private static void calcOxides(AllElementsOxide oxide2, int type, float weight, float al, float ba, float ca, float cl, float fe, float k, float mg, float na, float p, float s, float si, float ti) {

        AllElements ox = new AllElements();
        float phaseSum, weightDivPhaseSum;
        ox.setSI((float) (weight * si / (100 * 0.4675)));
        ox.setAL((float) (weight * al / (100 * 0.5292)));
        ox.setNA((float) (weight * na / (100 * 0.7420)));
        ox.setMG((float) (weight * mg / (100 * 0.6032)));
        ox.setP((float) (weight * p / (100 * 0.4364)));
        ox.setK((float) (weight * k / (100 * 0.8300)));
        ox.setCA((float) (weight * ca / (100 * 0.7146)));
        ox.setFE((float) (weight * fe / (100 * 0.6994))); // weight * fe2o3 fraction
        ox.setTI((float) (weight * ti / (100 * 0.5994)));//all conversion checked 4/4/94 SA
        ox.setBA((float) (weight * ba / (100 * 0.8956)));
        ox.setS((float) (weight * s / (100 * 0.4005)));
        ox.setCL((float) (weight * cl / (100 * 0.6890)));
        phaseSum = (ox.getSI() + ox.getAL() + ox.getCA()+ ox.getNA()+ ox.getMG()+ ox.getFE()+ ox.getBA()+ ox.getTI()+ ox.getCL()+ ox.getS()+ ox.getP()+ ox.getK());
        weightDivPhaseSum = weight + phaseSum;

        allElementsOxides.get(type).setSI(allElementsOxides.get(type).getSI() + weightDivPhaseSum * ox.getSI());
        allElementsOxides.get(type).setAL(allElementsOxides.get(type).getAL() + weightDivPhaseSum * ox.getAL());
        allElementsOxides.get(type).setNA(allElementsOxides.get(type).getNA() + weightDivPhaseSum * ox.getNA());
        allElementsOxides.get(type).setMG(allElementsOxides.get(type).getMG() + weightDivPhaseSum * ox.getMG());
        allElementsOxides.get(type).setP(allElementsOxides.get(type).getP() + weightDivPhaseSum * ox.getP());
        allElementsOxides.get(type).setK(allElementsOxides.get(type).getK() + weightDivPhaseSum * ox.getK());
        allElementsOxides.get(type).setS(allElementsOxides.get(type).getS() + weightDivPhaseSum * ox.getS());
        allElementsOxides.get(type).setCL(allElementsOxides.get(type).getCL() + weightDivPhaseSum * ox.getCL());
        allElementsOxides.get(type).setCA(allElementsOxides.get(type).getCA() + weightDivPhaseSum * ox.getCA());
        allElementsOxides.get(type).setFE(allElementsOxides.get(type).getFE() + weightDivPhaseSum * ox.getFE());
        allElementsOxides.get(type).setTI(allElementsOxides.get(type).getTI() + weightDivPhaseSum * ox.getTI());
        allElementsOxides.get(type).setBA(allElementsOxides.get(type).getBA() + weightDivPhaseSum * ox.getBA());
    }
    /*
     * Calculate weight
     *
     * Description: calculates new weights
     * all formulas are in form: original volume * original density * correction factor
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 7/27/94
     * Re-Modified by Binisha and copied in Java format
     *
     * * Company: Energy & Environmental Research Center
     * */
    private static float calcWeight(float areaCorrected, int type, float ca, float mg, float fe) {
        float correct, correct1, correct2;

        switch (type){
            case 1:// Quartz
                weight = (float) (areaCorrected * 2.65);
                break;
            case 2: // Iron Oxide
                weight = (float) (areaCorrected * 3.8 * 0.689 *0.64);
                //correction removes due to sem technique, Removed SA 7/18/94
                break;

            case 4: //Rutile
                weight = (float) (areaCorrected * 4.9);
                break;
            case 5: //Aluminum oxide
                weight = (float) (areaCorrected * 4);
                break;
            case 6: //calcite
                weight = (float) (areaCorrected * 2.8 * 0.561);
                break;
            case 7: //Dolomite
                correct = ca/ (ca + mg);
                correct2 = (float) ((correct * 0.561) + ((1-correct) * 0.489));
                weight = (float) (areaCorrected * 2.86 * correct2);
                break;
            case 8: // Ankerite
                correct = ca/ (ca + mg + fe);
                correct1 = mg / (mg + ca + fe);
                correct2 = (float) ((correct * 0.561) + (correct1 * 0.489) + ( 1- correct - correct1) * 0.689);
                weight = (float) (areaCorrected * 3 * correct2);
                break;
            case 9: //Kaolinite
                weight = (float) (areaCorrected * 2.65 * (2.23/ 2.65));
                break;
            case 10: //Montmorillonite
                weight = (float) (areaCorrected * 2.5 * (2.17/ 2.5));
                break;
            case 11: //K Al- Silicate
                weight = (float) (areaCorrected * 2.6 * (2.26/ 2.6));
                break;
            case 12: // Fe Al- Silicate
                weight = (float) (areaCorrected * 2.8);
                break;
            case 13:// ca Al- Silicate
                weight = (float) (areaCorrected * 2.65);
                break;
            case 14:// Na Al- Silicate
                weight = (float) (areaCorrected * 2.60);
                break;
            case 15: //Aluminosilicate
                weight = (float) (areaCorrected * 2.65);
                break;
            case 16://Mixed Al-Silicate
                weight = (float) (areaCorrected * 2.65);
                break;
            case 17: //Fe Silicate
                weight = (float) (areaCorrected * 4.4);
                break;
            case 18: //Ca Silicate
                weight = (float) (areaCorrected * 3.09);
                break;
            case 19: // Ca Aluminate
                weight = (float) (areaCorrected * 2.8);
                break;
            case 20: // Pyrite
                weight = (float) (areaCorrected * 5 * 2 * 0.64);
                break;
            case 21: // pyrrhotite
                weight = (float) (areaCorrected * 4.6 * 1.82 * 0.64);
                break;
            case 22: // oxidized pyrrhotite
                weight = (float) (areaCorrected * 5.3 * 1.67 * 0.64);
                break;
            case 23: // gypsum
                weight = (float) (areaCorrected * 2.5);
                break;
            case 24:// barite oxide
                weight = (float) (areaCorrected * 4.5* (5.72/ 4.5));
                break;
            case 25:// apatite
                weight = (float) (areaCorrected * 3.2);
                break;
            case 26: //Ca-Al-P
                weight = (float) (areaCorrected * 2.8);
                break;
            case 27://k-Cl
                weight = (float) (areaCorrected * ( 1.99 * 1.43));
                break;

            case 31:// Ca Rich
                weight = (float) (areaCorrected * 2.6);
                break;
            case 33: // All others
                weight = (float) (areaCorrected * 2.7);
                break;


        }//Switch
        phase[type] += weight;
        return weight;
    }//CalcWeight
    private static void zeroElements(AllElementsOxide elements) {
        elements = new AllElementsOxide();
        elements.setSI((float)0.0);
        elements.setAL((float)0.0);
        elements.setNA((float)0.0);
        elements.setMG((float)0.0);
        elements.setP((float)0.0);
        elements.setK((float)0.0);
        elements.setFE((float)0.0);
        elements.setCA((float)0.0);
        elements.setTI((float)0.0);
        elements.setBA((float)0.0);
        elements.setS((float)0.0);
        elements.setCL((float)0.0);
        allElementsOxides.add(elements);
    }
    /*
     * Adjust balance Organics CCS
     *
     * Description: Adds ccs and organics elements
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 7/27/94
     * Re-Modified by Binisha and copied in Java format
     *
     * * Company: Energy & Environmental Research Center
     * */
    private static void adjustBalOrgCcs() {


        if(allElementsORG.getNA()<0){

            allElementsCCS.setNA(allElementsCCS.getNA() +allElementsORG.getNA() );
            allElementsORG.setNA(0);

        }else if(allElementsORG.getMG()<0){
            allElementsCCS.setMG(allElementsCCS.getMG() +allElementsORG.getMG() );
            allElementsORG.setMG(0);

        }else if(allElementsORG.getSI()< 0){
            allElementsCCS.setSI(allElementsCCS.getSI() +allElementsORG.getSI() );

            allElementsORG.setSI(0);

        }else if(allElementsORG.getAL()< 0){
            allElementsCCS.setAL(allElementsCCS.getAL() +allElementsORG.getAL() );
            allElementsORG.setAL(0);

        }else if(allElementsORG.getP()< 0){
            allElementsCCS.setP(allElementsCCS.getP() +allElementsORG.getP() );
            allElementsORG.setP(0);

        }else if(allElementsORG.getCA()< 0){
            allElementsCCS.setCA(allElementsCCS.getCA() +allElementsORG.getCA() );
            allElementsORG.setCA(0);

        }else if(allElementsORG.getFE()< 0){
            allElementsCCS.setFE(allElementsCCS.getFE() +allElementsORG.getFE() );
            allElementsORG.setNA(0);

        }else if(allElementsORG.getTI()< 0){
            allElementsCCS.setTI(allElementsCCS.getTI() +allElementsORG.getTI() );
            allElementsORG.setTI(0);

        }

    }//adjustBalOrgCcs



/*data has to be orgnaized as the format documented so inserted into an array instead of repeating the same method multiple times*/
    private void writeChemFile(float[] xrf, AllElementsCCS allElementsCCS, AllElementsOrg allElementsORG) {

        double[] ccs = {allElementsCCS.getNA(), allElementsCCS.getMG(), allElementsCCS.getAL(), allElementsCCS.getSI(),
                allElementsCCS.getP(), allElementsCCS.getK(), allElementsCCS.getCA(), allElementsCCS.getFE(), allElementsCCS.getTI()};
        double[] org = {allElementsORG.getNA(), allElementsORG.getMG(), allElementsORG.getAL(), allElementsORG.getSI(),
                allElementsORG.getP(), allElementsORG.getK(), allElementsORG.getCA(), allElementsORG.getFE(), allElementsORG.getTI()};

        double[] data = {XRF[7], XRF[6],XRF[1],XRF[0],XRF[4],XRF[8],XRF[5],XRF[2],XRF[3]};

        for(int i = 0; i< data.length; i++){
            chemOutData.add(new ChemOutData( data[i], Float.parseFloat(df.format(ccs[i])),Float.parseFloat(df.format(org[i]))));
        }
    }




}
