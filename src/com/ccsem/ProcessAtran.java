package com.ccsem;

import com.ccsem.common.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.ccsem.Atran2.siTransDataList;
import static com.ccsem.Main.magnif;
/*
 * Atran1
 *
 * Description: Data Input, Data formation, Sulphur Removal & Fragmentation
 *
 * In the following character declarations, a 3-dimensional variable of the form a[b][c][d] and
 * 2 dimensional array of form e[f][g] have special meanings. b& f describe the number of blends,
 * and c represents the number of magnifications used. d&g represent the size of array(15).
 *
 *Also, this is the class that process all the other ATRAN files
 * * Author: Original by Tom Erickson, c version written by Shaker.
 * Modifications and testing done by Sean Allan 1994
 * Re-Modified by Binisha and copied in Java format
 * */
public class ProcessAtran {

    public static ArrayList<CCSEMStorageUnit> highLockedStorageUnits;
    public static ArrayList<CCSEMStorageUnit> highLiberatedStorageUnit;
    ArrayList<CCSEMStorageUnit> lowLockedStorageUnits;
    ArrayList<CCSEMStorageUnit> lowLiberatedStorageUnits;
    ArrayList<CCSEMStorageUnit> mainLiberatedStorageUnits = new ArrayList<>();
    ArrayList<CCSEMStorageUnit> mainLockedStorageUnits= new ArrayList<>();
    public static float maxSize, correction[] = new float[3];


    public static float[] totalWeight = new float[6], sitWeight = new float[6];

    Compound compound;
    GetPhase getPhase;
    float frager, frag1, frag2, frag3, frag4, frag5, frag6, frag7, frag9, frag10, newArea, newDiameter, normSum, freq, temp, na, mg, al, si, p, s, cl, k, ca, fe, ba, ti, area, avgDiameter;
    int coalEsce;
    FileIO fileio;
    Random randum;
    float LocLib, LocLibT, Frame, Point = 0;
   int[] hiPartSize = new int[3];
   int[] loPartSize = new int[3];
    public static int[] maxFrame = {0, 0, 0, 0, 0, 0};
    public static float peRcSiLoc = 0, percLoc = 0;
    public static ArrayList<MagGrType> magValue = new ArrayList<>();
    public static ArrayList<Coalescence> coalesData = new ArrayList<>();
    public static ArrayList<Coalescence> liberateData = new ArrayList<>();








    /*Read Magnification
     *
     * Description: "Mag.PRN" contains the following
     * 1. Number of magnification that will be used
     * 2. Magnification
     * 3. max. Particle dia will be processed. ( This is used for determining the Guard regions
     * 4. Loparticle size and Hiparticles sizes.
     *   The magnification should be specified in the decreasing order of magnification i.e highest to lowest magnification size
     *  (same as smallest to largest particles size). This size is output to a file SIZE.PRN. Later, Atran2 and Atran3 reads from
     *  this file and classifies the particle according to the size bin read. As the variable "size" is not needed anywhere else
     *  in the program, it will be declared locally. for detailed description on the classification see the "programmer Notes",
     *  if you meed the following criteria:
     * 1. You have access priviledge to such information and
     * 2. you can decipher Mr. C.R's handwriting.
     *
     * Pre condn:
     * num is assigned a pointer.
     *
     * post Condn:
     * *num contains the number of magnification regions.
     * magnif contains the number of magnification regions.
     * Magnif record holds all the magnification values and their guard regions
     * Hip holds the max. particle cutoff size for tht magnification
     * Lop holds the min. particle cutoff size for that magnification
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 1/23/96 hard coded in the mag.prn,
     * ATRAN only handles to magnification files and therefore no need to load mag.prn
     * Re-Modified by Binisha and copied in Java format
     *
     * Company: Energy & Environmental Research Center
     *
     *  /
     */

    public void AtranProcess(float coalEsce, String user, String fundNo, String boilerType, MagnificationServer magnificationServer, float[] sizeNumber) {


        compound  = new Compound();
        getPhase = new GetPhase();
        fileio = new FileIO();
        randum = new Random();
        randum.setSeed(123456789);
        hiPartSize = new int[]{10, 50};
        loPartSize = new int[]{1, 10};
        int[] magnifValue = {240, 50};
        int[] maxSize = {10 / 2, 100 / 2};

        for (int i = 0; i < 2; i++) {
            magnif = new MagGrType();
            magnif.setMag(magnifValue[i]);
            magnif.setGr(maxSize[i]);
            magValue.add(magnif);
        }

        highLockedStorageUnits = new ArrayList<>();
        highLiberatedStorageUnit = new ArrayList<>();
        lowLockedStorageUnits = new ArrayList<>();
        lowLiberatedStorageUnits = new ArrayList<>();
        ArrayList<CCSEMStorageUnit> highLock= new ArrayList<>();
        ArrayList<CCSEMStorageUnit> highLiberated = new ArrayList<>();
        ArrayList<CCSEMStorageUnit> lowLocked = new ArrayList<>();
        ArrayList<CCSEMStorageUnit> lowLiberated = new ArrayList<>();
        NormalizeData(magnificationServer.getMag50(), boilerType, "low",hiPartSize[0] , loPartSize[0]);
        NormalizeData(magnificationServer.getMag250(), boilerType, "high",hiPartSize[1] , loPartSize[1]);


        //HIMAGLO0.xls  loc:HILOCTY0.xls //LOMAGLO1.xls loc:LOLOCTY1.xls data representation

        lowLiberated =  FindPhase(lowLiberatedStorageUnits,0, 0);
        highLiberated =FindPhase(highLiberatedStorageUnit, 2, 1);
        lowLocked = FindPhase(lowLockedStorageUnits, 1, 0);
        highLock   = FindPhase(highLockedStorageUnits, 3, 1);


        mainLiberatedStorageUnits.addAll(highLiberated);
        mainLiberatedStorageUnits.addAll(lowLiberated);
        mainLockedStorageUnits.addAll(highLock);
        mainLockedStorageUnits.addAll(lowLocked);

        getPercentages(maxFrame);

        Atran5 atran5 = new Atran5();

        atran5.processAtran5(magnificationServer, correction);
        Atran2 atran2 = new Atran2();
        Atran3 atran3 = new Atran3();
        Atran6 atran6 = new Atran6();
        Atran4 atran4 = new Atran4();
        Atran7 atran7 = new Atran7();
//
        coalesData = atran2.ProcessAtran2(mainLockedStorageUnits, mainLockedStorageUnits.size(), lowLocked.size(), coalEsce, sizeNumber);
//
//
        liberateData = atran3.ProcessAtran3(mainLiberatedStorageUnits, mainLiberatedStorageUnits.size(), lowLiberated.size(), sizeNumber);
//
        coalesData = atran6.ProcessAtran6( coalesData, coalEsce, siTransDataList, percLoc, peRcSiLoc, boilerType, atran5.chemOutData);
        liberateData = atran6.ProcessAtran6Lib(liberateData);
//
//
//
        atran4.ProcessAtran4(magnificationServer, liberateData, coalesData,user, fundNo );
//
//
//
        atran7.ProcessAtran7(magnificationServer,percLoc, atran6.locPercT , boilerType , atran4.locAshDataArrayList, atran4.libAshDataArrayList);






    }
    /*Find Phase
     *
     * Description: Reads data from the HIMAGLO, HIMAGLI, LOMAGLO, LOMAGLI files first, outputs HILOCTY0, HILIBTYO,
     * LOLOCTYO AND LOLOBTYO FILES RESPECTIVITY. The phase type (1-33) is determined for each composition and then
     *  converted to an oxide for the output files
     * File inFile HIMAGLI0.xls & outFile  HILIBTY0.xls
     * File inFile LOMAGLI1.xls & outFile   LOLIBTY1.xls(double)
     * File inFile HIMAGLO0.xls  & outFile  HILOCTY0.xls
     * File inFile LOMAGLO1.xls & outFile  LOLOCTY1.xls(double)
     * By: Original by Tom Ericson, c version written by Shaker
     * Company: Energy & Environmental Research Center*/
    private ArrayList<CCSEMStorageUnit> FindPhase(ArrayList<CCSEMStorageUnit> highLiberatedStorageUnits, int totWeight, int maxFrm) {
       // ArrayList<CCSEMStorageUnit> highLiberatedStorageUnits = new ArrayList<>();
        float  normSum, temp, newAreaTimesDensity, diaratio;
        double[] density = {0.00, 2.65, 3.80, 3.61, 4.90, 4.00, 3.30, 3.45, 3.50, 2.23, 2.17, 2.60, 2.80, 2.65, 2.60, 2.65, 2.65, 4.40, 3.09, 2.80, 5.00, 4.60, 5.30, 2.50, 5.72, 3.20, 2.80, 1.99, 3.50, 2.60, 2.65, 2.60, 2.60, 2.70};


        for (int i = 0; i < highLiberatedStorageUnits.size(); i++) {
            if (highLiberatedStorageUnits.get(i).getAl() == 0) {
                highLiberatedStorageUnits.get(i).setAl((float) 0.001);
            }
            if (highLiberatedStorageUnits.get(i).getS() == 0) {
                highLiberatedStorageUnits.get(i).setS((float) 0.001);
            }
            highLiberatedStorageUnits.get(i).setPhase(getPhase.getPhase(highLiberatedStorageUnits.get(i).getAl(), highLiberatedStorageUnits.get(i).getBa(), highLiberatedStorageUnits.get(i).getCa(), highLiberatedStorageUnits.get(i).getCl(), highLiberatedStorageUnits.get(i).getFe(), highLiberatedStorageUnits.get(i).getK(), highLiberatedStorageUnits.get(i).getMg(), highLiberatedStorageUnits.get(i).getNA(), highLiberatedStorageUnits.get(i).getP(), highLiberatedStorageUnits.get(i).getS(), highLiberatedStorageUnits.get(i).getSi(), highLiberatedStorageUnits.get(i).getTi()));
            highLiberatedStorageUnits.get(i).setAl((float) (highLiberatedStorageUnits.get(i).getAl() / 0.5292));
            highLiberatedStorageUnits.get(i).setSi((float) (highLiberatedStorageUnits.get(i).getSi() / 0.4675));
            highLiberatedStorageUnits.get(i).setNA((float) (highLiberatedStorageUnits.get(i).getNA() / 0.7420));
            highLiberatedStorageUnits.get(i).setMg((float) (highLiberatedStorageUnits.get(i).getMg() / 0.6032));
            highLiberatedStorageUnits.get(i).setP((float) (highLiberatedStorageUnits.get(i).getP() / 0.4364));
            highLiberatedStorageUnits.get(i).setCl((float) (highLiberatedStorageUnits.get(i).getCl() / 0.6890));
            highLiberatedStorageUnits.get(i).setK((float) (highLiberatedStorageUnits.get(i).getK() / 0.8300));
            highLiberatedStorageUnits.get(i).setCa((float) (highLiberatedStorageUnits.get(i).getCa() / 0.7146));
            highLiberatedStorageUnits.get(i).setFe((float) (highLiberatedStorageUnits.get(i).getFe() / 0.6994));
            highLiberatedStorageUnits.get(i).setBa((float) (highLiberatedStorageUnits.get(i).getBa() / 0.8956));
            highLiberatedStorageUnits.get(i).setTi((float) (highLiberatedStorageUnits.get(i).getTi() / 0.5994));

            normSum = highLiberatedStorageUnits.get(i).getAl() + highLiberatedStorageUnits.get(i).getSi() + highLiberatedStorageUnits.get(i).getNA() + highLiberatedStorageUnits.get(i).getMg() + highLiberatedStorageUnits.get(i).getP() + highLiberatedStorageUnits.get(i).getCl() + highLiberatedStorageUnits.get(i).getK() + highLiberatedStorageUnits.get(i).getCa() + highLiberatedStorageUnits.get(i).getFe() + highLiberatedStorageUnits.get(i).getBa() + highLiberatedStorageUnits.get(i).getTi();
            temp = 100 / normSum;
            highLiberatedStorageUnits.get(i).setAl(highLiberatedStorageUnits.get(i).getAl() * temp);
            highLiberatedStorageUnits.get(i).setSi(highLiberatedStorageUnits.get(i).getSi() * temp);
            highLiberatedStorageUnits.get(i).setNA(highLiberatedStorageUnits.get(i).getNA() * temp);
            highLiberatedStorageUnits.get(i).setMg(highLiberatedStorageUnits.get(i).getMg() * temp);
            highLiberatedStorageUnits.get(i).setP(highLiberatedStorageUnits.get(i).getP() * temp);
            highLiberatedStorageUnits.get(i).setCl(highLiberatedStorageUnits.get(i).getCl() * temp);
            highLiberatedStorageUnits.get(i).setK(highLiberatedStorageUnits.get(i).getK() * temp);
            highLiberatedStorageUnits.get(i).setCa(highLiberatedStorageUnits.get(i).getCa() * temp);
            highLiberatedStorageUnits.get(i).setFe(highLiberatedStorageUnits.get(i).getFe() * temp);
            highLiberatedStorageUnits.get(i).setBa(highLiberatedStorageUnits.get(i).getBa() * temp);
            highLiberatedStorageUnits.get(i).setTi(highLiberatedStorageUnits.get(i).getTi() * temp);
            newAreaTimesDensity = (float) (highLiberatedStorageUnits.get(i).getArea() * density[(int) highLiberatedStorageUnits.get(i).getPhase()]);
            totalWeight[totWeight] += newAreaTimesDensity;
            sitWeight[totWeight] += newAreaTimesDensity * highLiberatedStorageUnits.get(i).getSi();
            if (highLiberatedStorageUnits.get(i).getFrame() > maxFrame[maxFrm]) {
                maxFrame[maxFrm] = highLiberatedStorageUnits.get(i).getFrame();
            }




        }
        return highLiberatedStorageUnits;
    }

    /*NormalizeData
     * A. Opens HIALLSU0, HIGMAGL00, HIGMAGLIB0 files
     * B. sets fragmentation numbers for Iron and others
     * C. Writes on LOALLSU1.xls, LOMAGLI1.xls & LOMAGLO1.xls
     *
     *
     * Modified: SA, 5-4-94, changes iron frag numbers
     * changed IFN from original 2200 to 100 for pcunit , 5/4/94
     * added iron decomposition function
     * */

    private void NormalizeData(List<CCSEMStorageUnit> storageUnits, String boilerType, String hiLow, int upperLimit, int lowerLimit) {
        if (boilerType.equals("Fluidized")) {
            frag1 = 20; //PYRITE
            frag2 = 20;// IRON CARBONATE
            frag3 = 20;// PYRRHOTITE & OX. PYRRHOTITE
            frag4 = 1;//GYPSUM
            frag5 = 1; // BARITE
            frag6 = 1; //CA-AL-P
            frag7 = 30; //KAOLINITE
            frag9 = 9; // CALCITE & DOLOMITE
            frag10 = 15; // QUATZ
            coalEsce = 70;
        } else if (boilerType.equals("Pulverized")) {
            frag1 = 5; //PYRITE
            frag2 = 5;// IRON CARBONATE
            frag3 = 5;// PYRRHOTITE & OX. PYRRHOTITE
            frag4 = 1;//GYPSUM
            frag5 = 1;// BARITE
            frag6 = 1;//CA-AL-P
            frag7 = 70; //KAOLINITE
            frag9 = 1; // CALCITE & DOLOMITE
            frag10 = 150;// QUATZ
            coalEsce = 150;
        } else if (boilerType.equals("Low")) {
            frag1 = 7;//PYRITE
            frag2 = 7;// IRON CARBONATE
            frag3 = 7;// PYRRHOTITE & OX. PYRRHOTITE
            frag4 = 1;//GYPSUM
            frag5 = 1;// BARITE
            frag6 = 1;//CA-AL-P
            frag7 = 20; //KAOLINITE
            frag9 = 1;// CALCITE & DOLOMITE
            frag10 = 25;// QUATZ
            coalEsce = 240;
        } else if (boilerType.equals("Cyclone")) {
            frag1 = 350;//PYRITE
            frag2 = 350;// IRON CARBONATE
            frag3 = 390;// PYRRHOTITE & OX. PYRRHOTITE
            frag4 = 1;//GYPSUM
            frag5 = 1;// BARITE
            frag6 = 1;//CA-AL-P
            frag7 = 100; //KAOLINITE
            frag9 = 1;// CALCITE & DOLOMITE
            frag10 = 1;// QUATZ
            coalEsce = 420;
        } else {
            System.out.println("No Boiler type found ");

        }



        for (int i = 0; i < storageUnits.size(); i++) {
              storageUnits.get(i).setPhase(getPhase.getPhase(storageUnits.get(i).getAl(), storageUnits.get(i).getBa(), storageUnits.get(i).getCa(), storageUnits.get(i).getCl(), storageUnits.get(i).getFe(), storageUnits.get(i).getK(), storageUnits.get(i).getMg(), storageUnits.get(i).getNA(), storageUnits.get(i).getP(), storageUnits.get(i).getS(), storageUnits.get(i).getSi(), storageUnits.get(i).getTi()));



            LocLibT = storageUnits.get(i).locLib;


            if ((storageUnits.get(i).getAvgDiameter() <= upperLimit) && storageUnits.get(i).getAvgDiameter() >= lowerLimit) {

                if (storageUnits.get(i).getS() < 0.1) {
                    storageUnits.get(i).setS((float) 0.1);

                }
                if (compound.pyrite(storageUnits.get(i).getBa(), storageUnits.get(i).getCa(), storageUnits.get(i).getFe(), storageUnits.get(i).getS())) {

                    storageUnits.get(i).setAvgDiameter((float) (storageUnits.get(i).getAvgDiameter() * 0.856 * 0.75));
                    storageUnits.get(i).setArea((float) (storageUnits.get(i).getArea() * 0.628));
                    storageUnits.get(i).setFrager(getFrager(frag1, 10));
                    storageUnits.get(i).setDiaRatio(storageUnits.get(i).getAvgDiameter() / storageUnits.get(i).getAvgDiameter()); // added by SA 9/19/94

                } else if (compound.ironCarbonate(storageUnits.get(i).getFe(), storageUnits.get(i).getS())) {
                    storageUnits.get(i).setFrager(getFrager(frag2, 10));
                    storageUnits.get(i).setAvgDiameter((float) (storageUnits.get(i).getAvgDiameter() * 0.7894 * 0.75));
                    storageUnits.get(i).setArea((float) (storageUnits.get(i).getArea() * 0.494));
                    storageUnits.get(i).setDiaRatio(storageUnits.get(i).getAvgDiameter() / storageUnits.get(i).getAvgDiameter());

                } else if (compound.pyrrhotiite(storageUnits.get(i).getBa(), storageUnits.get(i).getCa(), storageUnits.get(i).getFe(), storageUnits.get(i).getS())) {
                    storageUnits.get(i).setFrager(getFrager(frag3, (float) 1.5));
                    storageUnits.get(i).setAvgDiameter((float) (storageUnits.get(i).getAvgDiameter() * 0.888 * 0.75));
                    storageUnits.get(i).setArea((float) (storageUnits.get(i).getArea() * 0.700));
                    storageUnits.get(i).setDiaRatio(storageUnits.get(i).getAvgDiameter() / storageUnits.get(i).getAvgDiameter());

                } else if (compound.oxPyrrhotiite(storageUnits.get(i).getBa(), storageUnits.get(i).getCa(), storageUnits.get(i).getFe(), storageUnits.get(i).getS())) {
                    storageUnits.get(i).setFrager(getFrager(frag3, 2));
                    storageUnits.get(i).setAvgDiameter((float) (storageUnits.get(i).getAvgDiameter() * 0.968));
                    storageUnits.get(i).setArea((float) (storageUnits.get(i).getArea() * 0.908));
                    storageUnits.get(i).setDiaRatio(storageUnits.get(i).getAvgDiameter() / storageUnits.get(i).getAvgDiameter());

                } else if (compound.gypsum(storageUnits.get(i).getBa(), storageUnits.get(i).getCa(), storageUnits.get(i).getFe(), storageUnits.get(i).getS(), storageUnits.get(i).getSi(), storageUnits.get(i).getTi())) {
                    storageUnits.get(i).setFrager(getFrager(frag4, 2));
                    storageUnits.get(i).setAvgDiameter((float) (storageUnits.get(i).getAvgDiameter() * 0.7176));
                    storageUnits.get(i).setArea((float) (storageUnits.get(i).getArea() * 0.37));
                    storageUnits.get(i).setDiaRatio(storageUnits.get(i).getAvgDiameter() / storageUnits.get(i).getAvgDiameter());

                } else if (compound.barite(storageUnits.get(i).getBa(), storageUnits.get(i).getCa(), storageUnits.get(i).getFe(), storageUnits.get(i).getS(), storageUnits.get(i).getTi())) {
                    storageUnits.get(i).setFrager(getFrager(frag5, 2));
                    storageUnits.get(i).setAvgDiameter((float) (storageUnits.get(i).getAvgDiameter() * 0.803));
                    storageUnits.get(i).setArea((float) (storageUnits.get(i).getArea() * 0.517));
                    storageUnits.get(i).setDiaRatio(storageUnits.get(i).getAvgDiameter() / storageUnits.get(i).getAvgDiameter());

                } else if (compound.caAlP(storageUnits.get(i).getAl(), storageUnits.get(i).getCa(), storageUnits.get(i).getP(), storageUnits.get(i).getS(), storageUnits.get(i).getSi())) {
                    storageUnits.get(i).setFrager(getFrager(frag6, 2));
                    /*frequency correction due to binning */
                    freq = 1; // 2.0 changed to 1. 6/15/94
                    storageUnits.get(i).setAvgDiameter((float) (storageUnits.get(i).getAvgDiameter() * 0.75));
                    storageUnits.get(i).setArea((float) (storageUnits.get(i).getArea() * 0.65));
                    storageUnits.get(i).setDiaRatio(storageUnits.get(i).getAvgDiameter() / storageUnits.get(i).getAvgDiameter());

                } else if (compound.calcite(storageUnits.get(i).getAl(), storageUnits.get(i).getBa(), storageUnits.get(i).getCa(), storageUnits.get(i).getMg(), storageUnits.get(i).getP(), storageUnits.get(i).getS(), storageUnits.get(i).getSi(), storageUnits.get(i).getTi())) {
                    storageUnits.get(i).setFrager(getFrager(frag2, 2));
                    storageUnits.get(i).setAvgDiameter((float) (storageUnits.get(i).getAvgDiameter() * 0.784));
                    storageUnits.get(i).setArea((float) (storageUnits.get(i).getArea() * 0.475));
                    storageUnits.get(i).setDiaRatio(storageUnits.get(i).getAvgDiameter() / storageUnits.get(i).getAvgDiameter());

                } else if (compound.dolomite(storageUnits.get(i).getMg(), storageUnits.get(i).getCa())) {
                    storageUnits.get(i).setFrager(getFrager(frag9, 2));
                    storageUnits.get(i).setAvgDiameter((float) (storageUnits.get(i).getAvgDiameter() * 0.829));
                    storageUnits.get(i).setArea((float) (storageUnits.get(i).getArea() * 0.939));
                    storageUnits.get(i).setDiaRatio(storageUnits.get(i).getAvgDiameter() / storageUnits.get(i).getAvgDiameter());

                } else if (compound.kaolinite(storageUnits.get(i).getAl(), storageUnits.get(i).getCa(), storageUnits.get(i).getFe(), storageUnits.get(i).getK(), storageUnits.get(i).getNA(), storageUnits.get(i).getSi())) {
                    storageUnits.get(i).setFrager(getFrager(frag7, 2));
                    storageUnits.get(i).setAvgDiameter((float) (storageUnits.get(i).getAvgDiameter()));
                    storageUnits.get(i).setArea((float) (storageUnits.get(i).getArea()));
                    storageUnits.get(i).setDiaRatio(storageUnits.get(i).getAvgDiameter() / storageUnits.get(i).getAvgDiameter());

                } else if (compound.quartz(storageUnits.get(i).getAl(), storageUnits.get(i).getSi())) { //added 5/11/94 SA

                    storageUnits.get(i).setFrager(getFrager(frag10, frag10)); // using frag as divisor allows quartz to be fragmented bu the complete range
                    storageUnits.get(i).setAvgDiameter((float) (storageUnits.get(i).getAvgDiameter()));// no mult needed, mult is to account
                    storageUnits.get(i).setArea((float) (storageUnits.get(i).getArea()));// for loss of sulphur
                    storageUnits.get(i).setDiaRatio(storageUnits.get(i).getAvgDiameter() / storageUnits.get(i).getAvgDiameter());

                } else {
                    storageUnits.get(i).setFrager(1);
                    storageUnits.get(i).setAvgDiameter((float) (storageUnits.get(i).getAvgDiameter()));
                    storageUnits.get(i).setArea((float) (storageUnits.get(i).getArea()));
                    storageUnits.get(i).setDiaRatio(storageUnits.get(i).getAvgDiameter() / storageUnits.get(i).getAvgDiameter());
                }


                normSum = storageUnits.get(i).getAl() + storageUnits.get(i).getSi() + storageUnits.get(i).getNA() + storageUnits.get(i).getMg() + storageUnits.get(i).getP() + storageUnits.get(i).getCl() + storageUnits.get(i).getK() + storageUnits.get(i).getCa() + storageUnits.get(i).getFe() + storageUnits.get(i).getBa() + storageUnits.get(i).getTi();
                if (normSum >= 10) {
                    temp = 100 / normSum;
                    storageUnits.get(i).setAl(storageUnits.get(i).getAl() * temp);
                    storageUnits.get(i).setSi(storageUnits.get(i).getSi() * temp);
                    storageUnits.get(i).setNA(storageUnits.get(i).getNA() * temp);
                    storageUnits.get(i).setMg(storageUnits.get(i).getMg() * temp);
                    storageUnits.get(i).setP(storageUnits.get(i).getP() * temp);
                    storageUnits.get(i).setCl(storageUnits.get(i).getCl() * temp);
                    storageUnits.get(i).setK(storageUnits.get(i).getK() * temp);
                    storageUnits.get(i).setCa(storageUnits.get(i).getCa() * temp);
                    storageUnits.get(i).setFe(storageUnits.get(i).getFe() * temp);
                    storageUnits.get(i).setBa(storageUnits.get(i).getBa() * temp);
                    storageUnits.get(i).setTi(storageUnits.get(i).getTi() * temp);
                    storageUnits.get(i).setS((float) 0.0);
                    storageUnits.get(i).setLocLib((int) LocLibT);
                 //   writeMagFiletry(outFileName, storageUnits.get(i).getPoint(), storageUnits.get(i).getNA(), storageUnits.get(i).getMg(), storageUnits.get(i).getAl(), storageUnits.get(i).getSi(), storageUnits.get(i).getP(), storageUnits.get(i).getS(), storageUnits.get(i).getCl(), storageUnits.get(i).getK(), storageUnits.get(i).getCa(), storageUnits.get(i).getFe(), storageUnits.get(i).getBa(), storageUnits.get(i).getTi(), storageUnits.get(i).getNewDiameter(), storageUnits.get(i).getNewArea(), storageUnits.get(i).getFrame(), storageUnits.get(i).getLocLib(), storageUnits.get(i).getFrager(), storageUnits.get(i).getDiaRatio());



                    if (storageUnits.get(i).getLocLib() == 1) {
                        CCSEMStorageUnit lock = new CCSEMStorageUnit();



                            lock = writeMagData( storageUnits.get(i).getPoint(), storageUnits.get(i).getNA(), storageUnits.get(i).getMg(), storageUnits.get(i).getAl(), storageUnits.get(i).getSi(), storageUnits.get(i).getP(), storageUnits.get(i).getS(), storageUnits.get(i).getCl(), storageUnits.get(i).getK(), storageUnits.get(i).getCa(), storageUnits.get(i).getFe(), storageUnits.get(i).getBa(), storageUnits.get(i).getTi(), storageUnits.get(i).getAvgDiameter(), storageUnits.get(i).getArea(), storageUnits.get(i).getFrame(), storageUnits.get(i).getLocLib(), storageUnits.get(i).getFrager(), storageUnits.get(i).getDiaRatio());
                        if(hiLow=="low") {
                            lowLockedStorageUnits.add(lock);

                        }else{
                            highLockedStorageUnits.add(lock);
                        }

                    } else {
                        CCSEMStorageUnit lib = new CCSEMStorageUnit();

                            lib = writeMagData( storageUnits.get(i).getPoint(), storageUnits.get(i).getNA(), storageUnits.get(i).getMg(), storageUnits.get(i).getAl(), storageUnits.get(i).getSi(), storageUnits.get(i).getP(), storageUnits.get(i).getS(), storageUnits.get(i).getCl(), storageUnits.get(i).getK(), storageUnits.get(i).getCa(), storageUnits.get(i).getFe(), storageUnits.get(i).getBa(), storageUnits.get(i).getTi(), storageUnits.get(i).getAvgDiameter(), storageUnits.get(i).getArea(), storageUnits.get(i).getFrame(), storageUnits.get(i).getLocLib(), storageUnits.get(i).getFrager(), storageUnits.get(i).getDiaRatio());
                        if(hiLow=="low") {
                            lowLiberatedStorageUnits.add(lib);
                        }else {
                            highLiberatedStorageUnit.add(lib);
                        }
                    }
                }
            }

        }



    }

    /* GetPercentages

  Pre Condn:

  NumMag: denotes the number of magnifications
  Magnif: is a structure holding  the magnification  and guard region for each range
  MaxFrame: denotes the maximum frames for all the magnif. regions
  sitWeight: denotes the total silicon weight for each of the magnification region
  Percloc, Percsiloc: holds the total weight for locked and liberated phase of each
  magnif. range. x[2*n] contains the data for locked particles, whereas x[2*n + 1] contails the liberated
  particles data. Here X = {sitweiht, totalWeight}.

  Post condn:

  *percLoc contains percentage of locked particles
  percSiloc contains percentage of locked silicon particles.

  Task:
  This function first calculates the field size for each magnification. Then it determines
  the correction for each mag. range and output both field and correction to file "Field.prn".
  These values will be used in ATRAN5. It applies the correction to the locked and liberated
  weight (totalWeight [2*n] and totalWeight [2*n +1]). similarly, sitWeight is also normalized
  and this normalized data is used in calculating  Persiloc.

  Author: Original by Tom Erickson, c version written by Shaker.
  Modifications and testing done by Sean Allan 1994
  Re-Modified by Binisha and copied in Java format

  * Company: Energy & Environmental Research Center
   */
    private static void getPercentages(int[] maxFrame) {
        // TODO Auto-generated method stub
        float locWeight = 0, libWeight = 0, siLoc = 0, siLib = 0, numerator, field[] = new float[3];




        for (int i = 0; i < 2; i++) {
            float val = 84000 / magValue.get(i).getMag() - magValue.get(i).getGr();
            field[i] = (float) Math.pow(val, 2);
        }
        //numerator = maxFrame[numMag-1] * field[numMag-1];
        numerator = maxFrame[1] * field[1];
        for (int i = 0; i < 2; i++) {
            correction[i] = numerator / (maxFrame[i] * field[i]);

            locWeight += totalWeight[2 * i] * correction[i];
            libWeight += totalWeight[2 * i + 1] * correction[i];
            siLoc += sitWeight[2 * i] * correction[i];
            siLib += sitWeight[2 * i + 1] * correction[i];




        }
        percLoc = (locWeight / (locWeight + libWeight)) * 100;
        if (siLoc != 0 && siLib != 0) {
            peRcSiLoc = (siLoc / (siLoc + siLib)) * 100;

        } else
            peRcSiLoc = 0;
    }//GetPercentages
    private CCSEMStorageUnit writeMagData( int point, float na, float mg, float al, float si, float p, float s, float cl, float k, float ca, float fe, float ba, float ti, float newDiameter, float newArea, int frame, int locLib, float frager, float diaRatio) {

        CCSEMStorageUnit locked = new CCSEMStorageUnit();
        locked.setPoint(point);
        locked.setNA(na);
        locked.setMg(mg);
        locked.setAl(al);
        locked.setSi(si);
        locked.setP(p);
        locked.setS(s);
        locked.setCl(cl);
        locked.setK(k);
        locked.setCa(ca);
        locked.setFe(fe);
        locked.setBa(ba);
        locked.setTi(ti);
        locked.setAvgDiameter(newDiameter);
        locked.setArea(newArea);
        locked.setFrame(frame);
        locked.setLocLib(locLib);
        locked.setFrager(frager);
        locked.setDiaRatio(diaRatio);



//        lockedStorageUnits.add(locked);
        return locked     ;
    }
    /*Get Frager
     * Description: Determines the fragmentation numbers random returns a random number between 0 and (num-1)
     * Modified last: SA 9/6/94 */

    private float getFrager(float fragX, float divisor) {
        float temp;

        do {
            do {
                //temp = RANDOM.nextFloat(fragX + 1);
                temp = randum.nextInt((int) (fragX + 1));

            } while (temp < 1); // temp should be greater than 1
        } while (temp < ((int) Math.round(fragX / divisor)));

        return temp;

    }


}
