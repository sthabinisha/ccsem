package com.ccsem.common;

public class NameData {


    private String nameselement;

    private double myarray;

    public NameData(String nameselement, double myarray) {
        this.nameselement = nameselement;
        this.myarray = myarray;
    }

    public NameData(String nameselement) {
        this.nameselement = nameselement;
    }

    public NameData(double myarray) {
        this.myarray = myarray;
    }

    public double getMyarray() {
        return myarray;
    }

    public void setMyarray(double myarray) {
        this.myarray = myarray;
    }


    public String getNameselement() {
        return nameselement;
    }

    public void setNameselement(String nameselement) {
        this.nameselement = nameselement;
    }

}

