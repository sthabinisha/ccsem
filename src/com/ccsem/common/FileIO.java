package com.ccsem.common;

import java.io.*;
import java.util.Scanner;

public class FileIO {

	public File createFile(String fileName){
		File file = new File(fileName);
		try {
			if (file.createNewFile()) {
				System.out.println("File created: " + file.getName());
			} else{

			}
		} catch (IOException e) {
			e.printStackTrace();
		}


		return file;
	}
	public File tprintf (String fileName, String data ){
		File file = new File(fileName);
		try {
			if (file.createNewFile()) {
				System.out.println("File created: " + file.getName());
			} else {
				//System.out.println("File already exists.");
			}
			FileWriter myWriter = new FileWriter(fileName, true);

			myWriter.write(data);
			myWriter.write("\n");
			myWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}


		return file;
	}
    public float surfAreaSphere(float radius) {
		return (float) (4 * 3.14 * Math.pow(radius, 2));
	}

	public float volumeSphere(float r) {
		return (float) (4*22*r*r*r)/(3*7);
	}

	public void tprintfn(String s, String s1) {
		File file = new File(s);
		try {
			if (file.createNewFile()) {
				System.out.println("File created: " + file.getName());
			} else {
				//System.out.println("File already exists.");
			}
			FileWriter myWriter = new FileWriter(s, true);

			myWriter.write(s1+ " ");

			myWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void easierToreadforatran7(String s, String s1) {
		File file = new File(s);
		try {
			if (file.createNewFile()) {
				System.out.println("File created: " + file.getName());
			} else {
				//System.out.println("File already exists.");
			}
			FileWriter myWriter = new FileWriter(s, true);

			myWriter.write(s1+ " ");
			myWriter.write("\n");

			myWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public float crossAreaSphere(float rad) {
		return (float) (3.14 * Math.pow(rad, 2));
	}
}
