package com.ccsem.common;

public class SITransData {
    double siTrans, volumeTrans, SurfTrans;

    public SITransData(double siTrans, double volumeTrans, double surfTrans) {
        this.siTrans = siTrans;
        this.volumeTrans = volumeTrans;
        SurfTrans = surfTrans;
    }

    public double getSiTrans() {
        return siTrans;
    }

    public void setSiTrans(double siTrans) {
        this.siTrans = siTrans;
    }

    public double getVolumeTrans() {
        return volumeTrans;
    }

    public void setVolumeTrans(double volumeTrans) {
        this.volumeTrans = volumeTrans;
    }

    public double getSurfTrans() {
        return SurfTrans;
    }

    public void setSurfTrans(double surfTrans) {
        SurfTrans = surfTrans;
    }
}
