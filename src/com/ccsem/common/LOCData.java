package com.ccsem.common;

public class LOCData {
    float freq1, freq2, freq3, freq4, freq5, freq6, freq7;

    public LOCData(float freq1, float freq2, float freq3, float freq4, float freq5, float freq6, float freq7) {
        this.freq1 = freq1;
        this.freq2 = freq2;
        this.freq3 = freq3;
        this.freq4 = freq4;
        this.freq5 = freq5;
        this.freq6 = freq6;
        this.freq7 = freq7;
    }

    public float getFreq1() {
        return freq1;
    }

    public void setFreq1(float freq1) {
        this.freq1 = freq1;
    }

    public float getFreq2() {
        return freq2;
    }

    public void setFreq2(float freq2) {
        this.freq2 = freq2;
    }

    public float getFreq3() {
        return freq3;
    }

    public void setFreq3(float freq3) {
        this.freq3 = freq3;
    }

    public float getFreq4() {
        return freq4;
    }

    public void setFreq4(float freq4) {
        this.freq4 = freq4;
    }

    public float getFreq5() {
        return freq5;
    }

    public void setFreq5(float freq5) {
        this.freq5 = freq5;
    }

    public float getFreq6() {
        return freq6;
    }

    public void setFreq6(float freq6) {
        this.freq6 = freq6;
    }

    public float getFreq7() {
        return freq7;
    }

    public void setFreq7(float freq7) {
        this.freq7 = freq7;
    }
}
