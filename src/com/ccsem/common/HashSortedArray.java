package com.ccsem.common;
import java.util.Comparator;

public class HashSortedArray {
    public float getHashValue() {
        return hashValue;
    }

    public void setHashValue(float hashValue) {
        this.hashValue = hashValue;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }



    float hashValue;
    int index ;


    public static Comparator<HashSortedArray> hashSortedArrayComparator = new Comparator<HashSortedArray>() {

        @Override
        public int compare(HashSortedArray o1, HashSortedArray o2) {
        float hashVal = o1.getHashValue();
        float hashVal2 = o2.getHashValue();
        return (int) (hashVal-hashVal2);
        }
    };
    }
