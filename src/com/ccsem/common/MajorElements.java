package com.ccsem.common;

public class MajorElements {
	
	public float getNA() {
		return NA;
	}
	public void setNA(float nA) {
		NA = nA;
	}
	public float getMG() {
		return MG;
	}
	public void setMG(float mG) {
		MG = mG;
	}
	public float getAL() {
		return AL;
	}
	public void setAL(float aL) {
		AL = aL;
	}
	public float getSI() {
		return SI;
	}
	public void setSI(float sI) {
		SI = sI;
	}
	public float getP() {
		return P;
	}
	public void setP(float p) {
		P = p;
	}
	public float getS() {
		return S;
	}
	public void setS(float s) {
		S = s;
	}
	public float getCL() {
		return CL;
	}
	public void setCL(float cL) {
		CL = cL;
	}
	public float getK() {
		return K;
	}
	public void setK(float k) {
		K = k;
	}
	public float getCA() {
		return CA;
	}
	public void setCA(float cA) {
		CA = cA;
	}
	public float getFE() {
		return FE;
	}
	public void setFE(float fE) {
		FE = fE;
	}
	public float getBA() {
		return BA;
	}
	public void setBA(float bA) {
		BA = bA;
	}
	public float getTI() {
		return TI;
	}
	public void setTI(float tI) {
		TI = tI;
	}
	float NA;
	float MG;
	float AL;
	float SI;
	float P;
	float S;
	float CL;
	float K;
	float CA;
	float FE;
	float BA;
	float TI;

	public float getDiameter() {
		return diameter;
	}

	public void setDiameter(float diameter) {
		this.diameter = diameter;
	}

	public float getArea() {
		return area;
	}

	public void setArea(float area) {
		this.area = area;
	}

	public float getLocLib() {
		return locLib;
	}

	public void setLocLib(float locLib) {
		this.locLib = locLib;
	}

	public float getPhase() {
		return phase;
	}

	public void setPhase(float phase) {
		this.phase = phase;
	}

	public float getFrager() {
		return frager;
	}

	public void setFrager(float frager) {
		this.frager = frager;
	}

	public float getFrame() {
		return frame;
	}

	public void setFrame(float frame) {
		this.frame = frame;
	}

	public float getDiaRatio() {
		return diaRatio;
	}

	public void setDiaRatio(float diaRatio) {
		this.diaRatio = diaRatio;
	}

	public float getPoint() {
		return point;
	}

	public void setPoint(float point) {
		this.point = point;
	}

	public float diameter;
	public float area;
	public float locLib;
	public float phase;
	public float frager;
	public float frame;
	public float diaRatio;
	public float point;
	
	


	
	

	

}
