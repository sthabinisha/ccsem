package com.ccsem.common;

public class NameSizeData {

    private double size, partMax;
    private double sizeRange;
    private String sizeName;
    //private double[][][] myarray = new double[10][8][34];


    public NameSizeData(double size, double sizeRange) {
        this.size = size;
        //this.partMax = partMax;
        this.sizeRange = sizeRange;
        //this.sizeName = sizeName;
    }

    public NameSizeData(String sizeName) {
        this.sizeName = sizeName;
    }

    public double getSizeRange() {
        return sizeRange;
    }

    public void setSizeRange(double sizeRange) {
        this.sizeRange = sizeRange;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public double getPartMax() {
        return partMax;
    }

    public void setPartMax(double partMax) {
        this.partMax = partMax;
    }


}
