package com.ccsem.common;

public class GetPhase {
	double al, ca,  fe,  k,  na,  si, mg, p, s, cl, ba, ti;


	public int getPhase(double al, double ba, double ca, double cl, double fe, double k, double mg, double na, double p, double s, double si, double ti) {
		this.al = al;
		this. ba = ba;
		this.ca = ca;
		this.cl = cl;
		this.fe = fe;
		this.k = k;
		this.mg = mg;
		this.na = na;
		this.p = p;
		this.s = s;
		this.si = si;
		this.ti = ti;


		if(si >= 80.0 && al <= 5.0) {
			return 1;

		}else if(fe >= 80.0 && si <= 10.0 && s <= 5.0 && mg <= 5.0 && al <= 5.0) {
			return 2;

		}else if(mg >= 80.0 && ca <= 5.0){
			return 3;

		}else if(ba + ti >= 80.0 && s <= 5.0){
			return 4;


		}else if(al >= 80.0){
			return 5;

		}else if(ca >= 80.0 && s <= 10.0 && mg <= 5.0 && si <= 5.0 && p <= 5.0 && ti <= 5.0 && ba <= 5.0 && al <= 5.0){
			return 6;

		}else if(mg + ca >= 80.0 && mg > 5.0 && ca > 10.0){
			return 7;


		}else if(ca + fe + mg >= 80.0 && ca > 20.0 && s < 15.0 && fe > 20.0 && mg < fe){
			return 8;

		}else if(si + al >= 80.0 && 0.8 < si / al && si / al < 1.5 && k <= 5.0 && ca <= 5.0 && fe <= 5.0 && na <= 5.0){

			return 9;

		}else if(si + al >= 80.0 && 1.5 < si / al && si / al < 2.5 && k <= 5.0 && ca <= 5.0 && fe <= 5.0 && na <= 5.0){

			return 10;
		}else if(al + si + k >= 80.0 && k > 5.0 && si > 20.0 && al >= 15.0 && ca <= 5.0 && fe <= 5.0 && na <= 5.0){
			return 11;

		}else if(al + si + fe >= 80.0 && fe > 5.0 && si > 20.0 && al >= 15.0 && s <= 5.0 && ca <= 5.0 && k <= 5.0 && na <= 5.0){

			return 12;
		}else if(al + si + ca >= 80.0 && ca >= 5.0 && si > 20.0 && al >= 15.0 && na <= 5.0 && s <= 5.0 && k <= 5.0 && fe <= 5.0){

			return 13;
		}else if(k + al + si >= 80.0 && na >= 5.0 && s <= 5.0 && k <= 5.0 && ca <= 5.0 && fe <= 5.0 && al >= 15.0 && si > 20.0){

			return 14;
		}else if(al + si >= 80.0 &&  si > 20.0 && al > 20.0 && na <= 5.0 && k <= 5.0 && ca <= 5.0 && fe <= 5.0){

			return 15;
		}else if(na + al + si + k + ca + fe >= 80.0 && na < 10.0 && al > 20.0 && si > 20.0 && k < 10.0 && ca < 10.0 && fe < 10.0 && s <= 5.0){

			return 16;
		}else if(si + fe >= 80.0 && na <= 5.0 && s <= 5.0 && al <= 5.0 && k <= 5.0 && ca <= 5.0 && si > 20.0 && fe > 10.0){

			return 17;
		}else if(ca + si >= 80.0 && na <= 5.0 && al <= 5.0 && s <= 5.0 && k <= 5.0 && fe <= 5.0 && ca > 10.0 && si > 20.0){

			return 18;
		}else if(ca + al >= 80.0 && si <= 5.0 && s <= 5.0 && p <= 5.0 && al > 15.0 && ca > 20.0){
			return 19;

		}else if(fe + s >= 80.0 && fe >= 15.0 && fe / s <= 0.7 && s > 40.0 && ca < 10.0 && ba < 5.0){
			return  20;

		}else if(fe + s >= 80.0 && s>20 && fe >= 20.0 && fe / s > 0.7 && fe / s < 1.5 && s > 40.0 && ca < 10.0 && ba < 5.0){
			return 21;

		}else if(fe + s >= 80.0 && fe > 40.0 && fe / s >= 1.5 && s > 5.0 && ca < 10.0 && ba < 5.0){
			return 22;

		}else if(ca + s >= 80.0 && si < 10.0 && s > 20.0 && ca > 20.0 && ba < 10.0 && ti < 10.0){
			return 23;

		}else if(ba + s + ti >= 80.0 && fe < 10.0 && s > 20.0 && ca <= 5.0 && ba + ti > 20.0){
			return 24;

		}else if(ca + p >= 80.0 && p >= 20.0 && ca >= 20.0 && al <= 5.0 && s <= 5.0){
			return 25;

		}else if(al + p + ca >= 80.0 && al > 10.0 && p > 10.0 && ca > 10.0 && s <= 5.0 && si <= 5.0){
			return 26;

		}else if(cl + k >= 80.0 && cl >= 30.0 && k >= 30.0){
			return 27;

		}else if(ca + ba + s + ti >= 80.0 && fe <= 5.0 && s > 20.0 && ca > 5.0 && ba > 5.0 && ti > 5.0){
			return 28;

		}else if(ca + al + s + si >= 80.0 && ca > 5.0 && al > 5.0 && si > 5.0 && s > 5.0){
			return 29;

		}else if(si + ca >= 80.0 && ca >= 20.0 && si >= 20.0){
			return 32;

		}else if(ca >= 65.0 && ca < 80.0 && al < 15.0){
			return 31;


		}else if(si >= 65.0 && si <= 80){
			return 30;

		}else  {
			return 33;
		}

		}
}


