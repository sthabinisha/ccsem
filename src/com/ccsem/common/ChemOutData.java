package com.ccsem.common;

public class ChemOutData {

    private double xrf, allElementsCCS, allElementsOrg;

    public ChemOutData(double xrf, double allElementsCCS, double allElementsOrg) {
        this.xrf = xrf;
        this.allElementsCCS = allElementsCCS;
        this.allElementsOrg = allElementsOrg;
    }

    public double getXrf() {
        return xrf;
    }

    public void setXrf(double xrf) {
        this.xrf = xrf;
    }

    public double getAllElementsCCS() {
        return allElementsCCS;
    }

    public void setAllElementsCCS(double allElementsCCS) {
        this.allElementsCCS = allElementsCCS;
    }

    public double getAllElementsOrg() {
        return allElementsOrg;
    }

    public void setAllElementsOrg(double allElementsOrg) {
        this.allElementsOrg = allElementsOrg;
    }
}
