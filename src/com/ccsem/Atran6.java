package com.ccsem;

import com.ccsem.common.ChemOutData;
import com.ccsem.common.Coalescence;
import com.ccsem.common.FileIO;
import com.ccsem.common.SITransData;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;


/*
 * ATRAN 6
 *
 * Description: Condensation reactions also added in the organically associated and submicron particle
 *
 * * Author: Original by Tom Erickson, c version written by Shaker.
 * Modifications and testing done by Sean Allan 1994
 * Re-Modified by Binisha and copied in Java format
 * */
public class Atran6 {
    public static float[] libHetT = new float[10];
    public static float[] locHetT = new float[10];
    public static float[] homo = new float[10];
    public static float[] sub = new float[10];
    public static float[] locSubSerf = new float[5];
    public static float[] locPerc = new float[5];
    public static float[] libPerc = new float[5];
    public static float[] subPerc = new float[5];
    public static float[] size = new float[10];
    public static float[] avgDensity = new float[5];
    public static float[] blendRatio = new float[5];
    public static float[] siTrans = new float[2] ;
    public static float [] volumeTrans = new float[2];
    public static float [] surfTrans = new float[2];
    public static float[][] locHet = new float[5][10], libHet = new float[5][10];
    public static float percLoc, persiLoc, ratio, persiLib, locPercT, libPercT, subPercT, locSubserfT, libSurF, subSerf, ratio2, tottalDensity = 0, surfLoc = 0;
    public static Float coalEsce;
    public static int point;
    public static String[] subHomoFileName = {"subhomo0.prn", "subhomo1.prn", "subhomo2.prn", "subhomo3.prn", "subhomo4.prn"};
    public static String boilerType;
    public static double[] density = {0.0, 2.27, 3.58, 3.97, 2.65, 2.39, 2.32, 3.30, 5.30, 4.26};
    public static Coalescence storageUnit;
    public static int MAXPARTICLES;
    private static DecimalFormat df = new DecimalFormat("0.00");
    public static Random randum;
    private static final NumberFormat numberFormat = NumberFormat.getInstance();
    public static ArrayList<Coalescence> storageUnitsa = new ArrayList<>();
    ArrayList<Coalescence> LibrateFil = new ArrayList<>();

    public static ArrayList<Coalescence> Liberare = new ArrayList<>();
    File truePercFile;
    public static float minerals = 0, totalHomo = 0, totalSub = 0, totalhet = 0, locked = 0, subMicron = 0;
    public static float liberated;
    public static FileIO fileio;

    /*
     * ATRAN 6
     *
     * Description: inorganics cndensation
     *
     *
     * Modification: removed processed subhomofile function as blend is assigned to be 1
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 1994
     * Re-Modified by Binisha and copied in Java format
     * */

    public ArrayList<Coalescence> ProcessAtran6(ArrayList<Coalescence> liberateData, float coalEsce, List<SITransData> siTransDataList, float percLoc, float peRcSiLoc, String boilerType, List<ChemOutData> chemOutData) {

        randum = new Random();
        fileio = new FileIO();


        randum.setSeed(123456789);
        for (int i = 0; i < 10; i++) {
            homo[i] = 0;
            sub[i] = 0;
            libHetT[i] = 0;
            locHetT[i] = 0;


        }
        this.percLoc = percLoc;
        this.persiLoc = peRcSiLoc;

        siTrans[0] = (float) siTransDataList.get(0).getSiTrans();
        siTrans[1] = (float) siTransDataList.get(1).getSiTrans();
        surfTrans[0] = (float) siTransDataList.get(0).getSurfTrans();
        surfTrans[1] = (float) siTransDataList.get(1).getSurfTrans();
        this.libSurF = (float) surfTrans[1];



        persiLib = 100 -persiLoc;
        this.coalEsce = coalEsce;

        massforAllData(liberateData.size(), boilerType, chemOutData);
        storageUnitsa = processCoalescFile(liberateData);
        printSubHomo(subHomoFileName[0]);
        blendRatio[0] = 1;
        blendRatio[1] = 1;
        addLocandSub();
//        calcPercLocorLib();
        calcPercLocorLib(chemOutData);


        return storageUnitsa;
    }
    /*
     * Mass for all data
     *
     * Description: Determine the mass for the Liberated, locked and submicron particles using the SITRAN and the
     * HOMOHET files.
     * (HOMOHET files is missing)
     *
     * surfTrans[] = Total surface area (all particles)
     * siTrans[] = Total mass of si
     * PercSiLoc = % of Si that is locked
     * *Ratio = mass of total locked particles
     * *Ratio2 = mass of total liberated particles
     *
     * ** 4 MAIN PARTICLES MASS CLASSIFICATION***
     * Homo[] = mass of locked and liberated homogenous particles
     * sub[] = mass of submicron particles
     * LocHet[] = mass of locked heterogeneous particles
     * LinHet[] = mass of liberated heterogeneous particles
     *
     * Modifications: [[ Enter data , description of chg, initials]]
     // The function is incomplete
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 1994
     * Re-Modified by Binisha and copied in Java format
     * */
    private void massforAllData(int MAXPARTICLES, String boilerType, List<ChemOutData> chemOutData) {
        String homoHetFile = null;
        int orgSiValue = 1;
        this.boilerType = boilerType;
        double homoHet[][] = new double[10][4];
        if(Atran6.boilerType.equals("Fluidized")){ //Fluidized
            homoHetFile= "HOMOHET.FBC";
        }else if(Atran6.boilerType.equals("Pulverized")){
            homoHetFile= "HOMOHET.PCF";
        }else if(Atran6.boilerType.equals("Low")){
            homoHetFile= "HOMOHET.PCF";
        }else if(Atran6.boilerType.equals("Cyclone")){
            homoHetFile= "HOMOHET.CYC";
        }
        ratio = (float) ((orgSiValue * siTrans[0]/ (chemOutData.get(3).getXrf())) * (persiLoc/100));// 1087.9149
        ratio2 = (float) ((orgSiValue * siTrans[1]/ (chemOutData.get(3).getXrf())) * (persiLib/100));// 1109.7615


        homoHet = readHomoNuc(homoHetFile);

        for (int i = 1; i <=9 ; i++) {
            homoHet[i][1] /= 100;
            homoHet[i][2] /= 100;

            homoHet[i][3] = (homoHet[i][3]/100) - 0.0000001;






             homo[i] = (float) ((ratio + (/*ratio2 **/ ((coalEsce +1) /2))) * (chemOutData.get(i-1).getAllElementsOrg() * homoHet[i][1]));
            locHetT[i] = locHet[0][i] = (float) (ratio * (chemOutData.get(i-1).getAllElementsOrg() * homoHet[i][2]));
             libHetT[i] = libHet[0][i] = (float) (ratio2 * (chemOutData.get(i-1).getAllElementsOrg() * homoHet[i][2]));
        }
        // determining mass for each data set ---------
        for (int i = 1; i <=9 ; i++) {
            sub[i] = (float) (homo[i] * homoHet[i][3]);
            homo[i] = (float) (homo[i] * (1 - homoHet[i][3]));
            size[i]= 0;

        }
        for (int i = 1; i <= 9 ; i++) {
            if(sub[i] <0) sub[i]= 0;
            if(homo[i]<0) homo[i]= 0;

        }
        //determines homogeneous radius for each composition---
        for (int i = 1; i <=9 ; i++) {


            double test =  ((homo[i] / (MAXPARTICLES * ((coalEsce + 1)/2))) /density[i]) *(3 / (4* (22/7)));
            //System.out.println(coalEsce+ " test "+ test);
            size[i] = (float) Math.pow(test, (1/3));

        }
    }

    /*
     * Get Input data  -- method deleted
     *
     * Description: Loads siTrans and percloc files into arrays
     *
     *
     * Modifications: [[Enter data, description of chg, initials]]
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 1994
     * Re-Modified by Binisha and copied in Java format
     * */

    /*
     * Process Coalesce File
     *
     * Description: Reads in coalesce file from Atran2, determines the new particle colume, weight and diameter are
     * determined after condensation
     *
     * Modification: SA, 7/11/94, changes IrandomNo() to random(), random(num) return num-1, there fpr need to random(num+1)
     * dude = (coaslesce+1) * surfer => coalesce * surfer
     * ntc<=1 => ntc < 1 ntc = 1
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 1994
     * Re-Modified by Binisha and copied in Java format
     * */
    private static ArrayList<Coalescence> processCoalescFile(ArrayList<Coalescence> storageUnits) {
        //storageUnits = processCoalesceclsFile();
        //MAXPARTICLES = storageUnits.size();
        double diam, PV, surface, surfer, dude, wtall, volumeAll, addDensity, partVolume, partWeight, partPerc, temp, newDiam, totalalStuff = 0, newVolume, newDensity;
        int ntc;
        int[] last = new int[10];
        double[] wt = new double[10], vol = new double[10];


        for (int i = 0; i < storageUnits.size(); i++) {


            surface = fileio.surfAreaSphere(storageUnits.get(i).getAvgdiameter()/2);
            surfer = (surface / (surfTrans[0]/ MAXPARTICLES));
            dude = coalEsce * surfer;


            ntc = (int) dude;


            if((dude - ntc) >= 0.5){
                ntc+=1;
            }
            if(ntc<1)
                ntc = 1;
            for (int j = 1; j <= 9; j++) {
                do{
                    last[j] = randum.nextInt(ntc*2);
                }while((last[j]> ntc) || (last[j] <=0));

                wt[j] = 0;
                vol[j] = 0;

            }
            wtall =  0.000001;
            volumeAll = 0;
            addDensity = 0;
            for (int j = 1; j <=9 ; j++) {
                vol[j] = last[j] * 1.3 * (22/7) * (size[j]*size[j]*size[j] );
                wt[j] = vol[j] * density[j];
                volumeAll += vol[j];
                addDensity += wt[j] * density[j];

            }
            addDensity /= wtall;
            partVolume = fileio.volumeSphere(storageUnits.get(i).getAvgdiameter()/2);
            partWeight = partVolume * storageUnits.get(i).getAvgDensity();

            partPerc = partWeight / (partWeight + wtall);


            newVolume = (partVolume + volumeAll);
            newDensity=  (partPerc * storageUnits.get(i).getAvgDensity() +( (1-partPerc) * addDensity));
            temp = 100 * ( 1 - partPerc);
            storageUnits.get(i).setNA((float) ( storageUnits.get(i).getNA()* partPerc + (wt[1]/wtall) * temp));
            storageUnits.get(i).setMg((float) ( storageUnits.get(i).getMg()* partPerc +( wt[2]/wtall) * temp));
            storageUnits.get(i).setAl((float) ( storageUnits.get(i).getAl()* partPerc + (wt[3]/wtall) * temp));
            storageUnits.get(i).setSi((float) ( storageUnits.get(i).getSi()* partPerc + (wt[4]/wtall) * temp));
            storageUnits.get(i).setP((float) ( storageUnits.get(i).getP()* partPerc + (wt[5]/wtall) * temp));


            storageUnits.get(i).setS( (float)( storageUnits.get(i).getS()* partPerc ));
            storageUnits.get(i).setCl((float) (storageUnits.get(i).getCl()* partPerc));
            storageUnits.get(i).setK((float) ( storageUnits.get(i).getK()* partPerc + (wt[6]/wtall) * temp));
            storageUnits.get(i).setCa((float) ( storageUnits.get(i).getCa()* partPerc + (wt[7]/wtall) * temp));
            storageUnits.get(i).setFe((float) ( storageUnits.get(i).getFe()* partPerc + (wt[8]/wtall) * temp));
            storageUnits.get(i).setBa( (float)( storageUnits.get(i).getBa()* partPerc ));
            storageUnits.get(i).setTi((float) ( storageUnits.get(i).getTi()* partPerc + (wt[9]/wtall) * temp));
            //(2 * Math.pow(((3 * volume) / 12.56), 0.33))
            float check = (float) (newVolume * 0.23863636363);
            newDiam =( Math.pow((newVolume* 0.23863), 0.333))* 2;
            storageUnits.get(i).setAvgdiameter((float) newDiam);
            storageUnits.get(i).setMaxDiameter((float) newDiam);
            storageUnits.get(i).setMinDiameter((float) newDiam);
            storageUnits.get(i).setAvgDensity((float) newDensity);
            storageUnits.get(i).setPv((float) newVolume);







            tottalDensity += storageUnits.get(i).getAvgDensity();
            surfLoc +=  fileio.surfAreaSphere((float) newDiam/2);
            totalalStuff = totalalStuff + surface;




        }
        return storageUnits;
    }

    /*
     * Print Submicron Homogenous
     *
     * Description: Totals the submicron amounts and prints it in the following format number, na, mg, al, si, p, s, cl
     * k, ca, fe, ba, ti, diam, area, subdensity
     *
     * cals=> (x * locked mass si)     (y * Liberated Mass Si)
     *        --------------------   = ------------------------
     *         Perc Si Locked           Perc Si Liberated
     * x * Locked mass Si = siTrans[0]
     * y * Liberated mass Si = Sitrans[1]
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 1994
     * Re-Modified by Binisha and copied in Java format
     * */
    private static void printSubHomo(String subHomoFileName) {
        double nas = 0, mgs = 0, als = 0, sis = 0, ps = 0, ss = 0, cls = 0, ks = 0, cas = 0, fes = 0, bas = 0,  tis = 0, diam = 0, subVolume = 0, area = 0, bigSurf = 0, calc = 0, subTotal = 0, subDensity = 0, number = 0; ;
        File subHomoFile = fileio.createFile(subHomoFileName);
        if(subHomoFile.length()!= 0){
            subHomoFile.delete();
            subHomoFile = fileio.createFile(subHomoFileName);
        }
        String subMicFile = "SUBMIC.PRN";
        File micFile = fileio.createFile(subMicFile);

        if (micFile.length()!=0) {
            micFile.delete();
            micFile = fileio.createFile(subMicFile);
        }
        for (int i = 1; i <= 9; ++i) {
            subTotal += sub[i]; // total amount of submicron elements
            subDensity = subDensity + sub[i] * density[i];

        }
        if(subTotal == 0)
            subTotal = 1;
        if(subTotal> 0){
            System.out.println(df.format(subTotal)+ " sub total");// 4428688.94 sub total

            subDensity = subDensity / subTotal;
            System.out.println(subDensity+ " sub density");//2.787656231594175 sub density

            nas = sub[1]/ subTotal * 100;
            mgs = sub[2]/ subTotal * 100;
            als = sub[3]/ subTotal * 100;
            sis = sub[4]/ subTotal * 100;
            ps = sub[5]/ subTotal * 100;
            ss = 0;
            cls = 0;
            ks = sub[6]/ subTotal * 100;
            cas = sub[7]/ subTotal * 100;
            fes = sub[8]/ subTotal * 100;
            bas = 0;
            tis = sub[9]/ subTotal * 100;

            diam = 0.5;
            subVolume = fileio.volumeSphere((float) diam/2);// volume of sphere
            area = fileio.crossAreaSphere((float) diam/2);// cross sectional area of sphere
            // this is correct, read in atran 6 and it expects cross area
            number = ((subTotal / subDensity) / subVolume);
            System.out.println(df.format(number) + "num");// 24263448.95



        }
        writeSubHomoFile(subHomoFileName, number, nas, mgs, als, sis, ps, ss, cls, ks, cas, fes, bas, tis, diam, area, subDensity);
        writeSubMicFile(subMicFile, number, nas, mgs, als, sis, ps, ss, cls, ks, cas, fes, bas, tis, diam, subVolume, subDensity);
        subSerf = (float) (number * fileio.surfAreaSphere((float) diam/2)); // area of sphere
        //System.out.println(fileio.surfAreaSphere((float) diam/2)+ " surf area");
        //determining area % for Lib, Loc and Sub
        calc = (siTrans[0] - (persiLoc/100 * siTrans[0]))/ (persiLoc /100 * siTrans[1]);
        bigSurf = surfTrans[0] + subSerf + calc * surfTrans[1];
        locPercT = Float.valueOf(df.format(locPerc[0] = (float) (surfTrans[0]/bigSurf)));
        libPercT =  Float.valueOf(df.format(libPerc[0] = (float) ((surfTrans[1] * calc)/bigSurf)));
        subPercT = Float.valueOf(df.format( subPerc[0] = (float) (subSerf / bigSurf)));
        locSubserfT =  Float.valueOf(df.format(locSubSerf[0] = (surfLoc + subSerf)));
        if(point != 0)
            avgDensity[0] = tottalDensity/point;
        else
            avgDensity[0] = 0;

    }
    /*
     * Write and reprocess subMicron files
     *
     * Description: Adds together locked and submicron particles, then determines new composition and volume
     *
     * number = (Total Mass submicron/totaldensity )/totalvolume)
     *        = #of submicron particles
     * Modification: Binisha: added into after writingg subHomofiles as it allows to not go through the same file again
     * Suggestion to future: we can use only one file instead of writing the same data into different files

     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 1994
     * Re-Modified by Binisha and copied in Java format
     * */
    private static void writeSubMicFile(String subMicFile, double number, double nas, double mgs, double als, double sis, double ps, double ss, double cls, double ks, double cas, double fes, double bas, double tis, double diam, double subVolume, double subDensity) {
        try {
            File outputFile = new File(subMicFile);
            FileWriter myWriter = new FileWriter(outputFile, true);

            myWriter.write(df.format(number) + " "+df.format( nas)+ " "+ df.format(mgs) + " "+ df.format(als) + " "+ df.format(sis) + " "+ df.format(ps)+ " "+ df.format(ss)+ " "+ df.format(cls) + " "+ df.format(ks) + " "+ df.format(cas) + " "+ df.format(fes) + " "+ df.format(bas) + " "+ df.format(tis)+ " "+ df.format(diam) + " "+df.format(subVolume) + " "+ df.format(subDensity) );
            myWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void writeSubHomoFile(String subHomoFileName, double number, double nas, double mgs, double als, double sis, double ps, double ss, double cls, double ks, double cas, double fes, double bas, double tis, double diam, double area, double subDensity) {
        try {
            File outputFile = new File(subHomoFileName);
            FileWriter myWriter = new FileWriter(outputFile, true);

            myWriter.write(df.format(number) + " "+df.format( nas)+ " "+ df.format(mgs) + " "+ df.format(als) + " "+ df.format(sis) + " "+ df.format(ps)+ " "+ df.format(ss)+ " "+ df.format(cls) + " "+ df.format(ks) + " "+ df.format(cas) + " "+ df.format(fes) + " "+ df.format(bas) + " "+ df.format(tis)+ " "+ df.format(diam) + " "+df.format(area) + " "+ df.format(subDensity) );
            myWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private static void addLocandSub() {
        float partSurf, weight, volume, partVolume, partPerc, temp, newDiam, perim, newDensity, diam, diam2, diam3, involumer, outVolume, shape, partweight, naf, mgf, alf, sif, pf, sf, clf, kf, caf, fef, baf, tif;
        ;
        int frameI;
        // int points, counts, comp, frameI, na, mg, al, si, p, s, cl, k, ca, fe, ba, ti, frame, xcoord, ycoord;
        float[] adweight = new float[10], addVolume = new float[10];

        for (int i = 0; i < storageUnitsa.size(); i++) {
            partSurf = fileio.surfAreaSphere(storageUnitsa.get(i).getAvgdiameter()/2);
            weight = 0;
            volume = 0;
            for (int j = 1; j <= 9; ++j) {
                adweight[j] = (partSurf / locSubserfT) * locHetT[j];
                addVolume[j] = (float) (adweight[j] / density[j]);
                weight = weight + adweight[j];
                volume = volume + addVolume[j];


            }
            partVolume = fileio.volumeSphere(storageUnitsa.get(i).getAvgdiameter()/2);
            partweight = partVolume * (storageUnitsa.get(i).getAvgDensity());
            partPerc = partweight / (partweight + weight);
            temp = 100 * (1 - partPerc);
            if (weight == 0) {
                storageUnitsa.get(i).setNA( storageUnitsa.get(i).getNA());
                storageUnitsa.get(i).setMg(storageUnitsa.get(i).getMg());
                storageUnitsa.get(i).setAl( storageUnitsa.get(i).getAl());
                storageUnitsa.get(i).setSi(storageUnitsa.get(i).getSi());
                storageUnitsa.get(i).setP( storageUnitsa.get(i).getP());
                storageUnitsa.get(i).setS(storageUnitsa.get(i).getS());
                storageUnitsa.get(i).setCl( storageUnitsa.get(i).getCl());
                storageUnitsa.get(i).setK( storageUnitsa.get(i).getK());
                storageUnitsa.get(i).setCa(storageUnitsa.get(i).getCa());
                storageUnitsa.get(i).setFe( storageUnitsa.get(i).getFe());
                storageUnitsa.get(i).setBa( storageUnitsa.get(i).getBa());
                storageUnitsa.get(i).setTi( storageUnitsa.get(i).getTi());

            } else {
                storageUnitsa.get(i).setNA( storageUnitsa.get(i).getNA() * partPerc + (adweight[1] / weight) * temp);
                storageUnitsa.get(i).setMg(storageUnitsa.get(i).getMg() * partPerc + (adweight[2] / weight) * temp);
                storageUnitsa.get(i).setAl(storageUnitsa.get(i).getAl() * partPerc + (adweight[3] / weight) * temp);
                storageUnitsa.get(i).setSi(storageUnitsa.get(i).getSi() * partPerc + (adweight[4] / weight) * temp);
                storageUnitsa.get(i).setP(storageUnitsa.get(i).getP() * partPerc + (adweight[5] / weight) * temp);
                storageUnitsa.get(i).setS(storageUnitsa.get(i).getS() * partPerc);
                storageUnitsa.get(i).setCl(storageUnitsa.get(i).getCl() * partPerc);
                storageUnitsa.get(i).setK( storageUnitsa.get(i).getK() * partPerc + (adweight[6] / weight) * temp);
                storageUnitsa.get(i).setCa(storageUnitsa.get(i).getCa() * partPerc + (adweight[7] / weight) * temp);
                storageUnitsa.get(i).setFe(storageUnitsa.get(i).getFe() * partPerc + (adweight[8] / weight) * temp);
                storageUnitsa.get(i).setBa(  storageUnitsa.get(i).getBa() * partPerc);
                storageUnitsa.get(i).setTi( storageUnitsa.get(i).getTi() * partPerc + (adweight[9] / weight) * temp);
            }

            storageUnitsa.get(i).setAvgdiameter((float) ((Math.pow(((volume + partVolume) * (3 / 12.56)),0.333)) * 2));
            storageUnitsa.get(i).setMaxDiameter((float) ((Math.pow(((volume + partVolume) * (3 / 12.56)),0.333)) * 2));
            storageUnitsa.get(i).setMinDiameter((float) ((Math.pow(((volume + partVolume) * (3 / 12.56)),0.333)) * 2));

            storageUnitsa.get(i).setPv(fileio.volumeSphere(storageUnitsa.get(i).getAvgdiameter()/2));
            storageUnitsa.get(i).setAvgDensity((partweight + weight) / (volume + partVolume));
            storageUnitsa.get(i).setFrame(storageUnitsa.get(i).getFrame());


        }



    }

    /*
     * Calc Perc Loc or Lib
     *
     * Description: Percent of locked, submicron and liberated particles determined
     *
     *
     * Modification: SA, 7/13/94, added ratio1 to demoninator in calculation of TotalHomo, TotalSub, Totalhet.
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 1994
     * Re-Modified by Binisha and copied in Java format
     * */
    private static void calcPercLocorLib(List<ChemOutData> chemOutData) {


        for (int i = 1; i <= 9 ; ++i) {
            minerals = Float.valueOf(df.format(minerals+ chemOutData.get(i-1).getAllElementsCCS()));

            if ((ratio + ratio2) != 0) {
                totalHomo = Float.valueOf(df.format(totalHomo+ homo[i] / (ratio + ratio2 * ((coalEsce + 1) / 2))));
                totalSub = Float.valueOf(df.format(totalSub+ (sub[i] / Float.valueOf(df.format((ratio + ratio2 * ((coalEsce + 1) / 2)))))));
                if (ratio != 0) {
                    totalhet = Float.valueOf(totalhet+ locHetT[i] / ratio);

                }

            }
        }
        subMicron = Float.valueOf(df.format(((totalSub + subPercT * totalhet))));
        // locked =Float.valueOf(df.format((percLoc / 100) * minerals ))+ totalHomo + Float.valueOf(df.format(locPercT * totalhet));
        String mult = df.format((locPercT * totalhet));
        String percMult = df.format((percLoc/100) * minerals);
        locked = Float.valueOf(df.format((Float.valueOf(percMult)+ totalHomo+ Float.valueOf(mult))));

        //Float.valueOf(df.format(locPercT * totalhet)) ;
        // locked = locked +Double.parseDouble(df.format(locPercT)) * Double.parseDouble(df.format(totalhet));
        if ((locked + subMicron) > 100) {


            liberated = (float) (100.0- locked);


            liberated = (liberated * ((100 - subMicron) / 100));
            locked = locked * ((100 - subMicron) / 100);
        }







    }
    /*
     * Addtoliberated particles
     *
     * Description: Adds together liberated and vapor species particles, then determines new composition and volume
     * LibSurfT = siTrans[1] (total liberated si surface area)
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 1994
     * Re-Modified by Binisha and copied in Java format
     * */
    public ArrayList<Coalescence> ProcessAtran6Lib(ArrayList<Coalescence> LibrateFil) {
        float partSurf, weight, volume, partVolume, partPerc, temp, newDiam, perim, newDensity, diam, diam2, diam3, involumer, outVolume, shape, partweight, naf, mgf, alf, sif, pf, sf, clf, kf, caf, fef, baf, tif;
        ;
        int frameI;
        // int points, counts, comp, frameI, na, mg, al, si, p, s, cl, k, ca, fe, ba, ti, frame, xcoord, ycoord;
        float[] adweight = new float[10], addVolume = new float[10];


        for (int i = 0; i < LibrateFil.size(); i++) {
            partSurf = fileio.surfAreaSphere(LibrateFil.get(i).getAvgdiameter()/2);
            weight = 0;
            volume = 0;
            for (int j = 1; j <= 9; ++j) {
                adweight[j] = (partSurf / libSurF) * libHetT[j];
                addVolume[j] = (float) (adweight[j] / density[j]);
                weight = weight + adweight[j];
                volume = volume + addVolume[j];


            }
            partVolume = fileio.volumeSphere(LibrateFil.get(i).getAvgdiameter()/2);
            partweight = partVolume * (LibrateFil.get(i).getAvgDensity());
            partPerc = partweight / (partweight + weight);
            temp = 100 * (1 - partPerc);
            if (weight == 0) {
                LibrateFil.get(i).setNA(LibrateFil.get(i).getNA());;
                LibrateFil.get(i).setMg(LibrateFil.get(i).getMg());
                LibrateFil.get(i).setAl(LibrateFil.get(i).getAl());
                LibrateFil.get(i).setSi(LibrateFil.get(i).getSi());
                LibrateFil.get(i).setP(LibrateFil.get(i).getP());
                LibrateFil.get(i).setS(LibrateFil.get(i).getS());
                LibrateFil.get(i).setCl(LibrateFil.get(i).getCl());
                LibrateFil.get(i).setK(LibrateFil.get(i).getK());
                LibrateFil.get(i).setCa(LibrateFil.get(i).getCa());
                LibrateFil.get(i).setFe(LibrateFil.get(i).getFe());
                LibrateFil.get(i).setBa( LibrateFil.get(i).getBa());
                LibrateFil.get(i).setTi(LibrateFil.get(i).getTi());

            } else {
                LibrateFil.get(i).setNA(LibrateFil.get(i).getNA() * partPerc + (adweight[1] / weight) * temp);
                LibrateFil.get(i).setMg(LibrateFil.get(i).getMg() * partPerc + (adweight[2] / weight) * temp);
                LibrateFil.get(i).setAl(LibrateFil.get(i).getAl() * partPerc + (adweight[3] / weight) * temp);
                LibrateFil.get(i).setSi(LibrateFil.get(i).getSi() * partPerc + (adweight[4] / weight) * temp);
                LibrateFil.get(i).setP(LibrateFil.get(i).getP() * partPerc + (adweight[5] / weight) * temp);
                LibrateFil.get(i).setS(LibrateFil.get(i).getS() * partPerc);
                LibrateFil.get(i).setCl(LibrateFil.get(i).getCl() * partPerc);
                LibrateFil.get(i).setK(LibrateFil.get(i).getK() * partPerc + (adweight[6] / weight) * temp);
                LibrateFil.get(i).setCa(LibrateFil.get(i).getCa() * partPerc + (adweight[7] / weight) * temp);
                LibrateFil.get(i).setFe(LibrateFil.get(i).getFe() * partPerc + (adweight[8] / weight) * temp);
                LibrateFil.get(i).setBa(LibrateFil.get(i).getBa() * partPerc);
                LibrateFil.get(i).setTi(LibrateFil.get(i).getTi() * partPerc + (adweight[9] / weight) * temp);
            }

            newDiam = (float) (Math.pow(((volume + partVolume) * 3 / 12.56), (0.333)) )* 2;
            LibrateFil.get(i).setAvgdiameter((float) newDiam);
            LibrateFil.get(i).setMaxDiameter((float) newDiam);
            LibrateFil.get(i).setMinDiameter((float) newDiam);

            outVolume = fileio.volumeSphere(newDiam/2);
            newDensity = ((partweight + weight) / (volume + partVolume));
            frameI = LibrateFil.get(i).getFrame();
            //LibrateFil.get(i).setAvgDensity((float) newDiam);
            LibrateFil.get(i).setPv((float) outVolume);
            LibrateFil.get(i).setFrame( frameI);




        }

        return LibrateFil;
    }
    /*
     * Reads HomoHetFile, 3 colums -> Homogenous Nuculation, Heteregenous Nuculation and Submicron
     *
     * Description: Reads the data that has been manually inputted 10/80/10 2020
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 1994
     * Re-Modified by Binisha and copied in Java format
     * */
    private static double[][] readHomoNuc(String homoHetFile) {
        double homoHet[][] = new double[10][4];

        try{
            File file = new File(homoHetFile);
            Scanner scanner = new Scanner(file);
            int i = 1, j= 1;
            while (scanner.hasNextFloat()) {
                homoHet[i][1] = scanner.nextFloat();
                homoHet[i][2] = scanner.nextFloat();
                homoHet[i][3] = scanner.nextFloat();

                i++;
            }

        }catch (Exception e){
            System.err.println("Error: " + e.getMessage());
        }
        return homoHet;
    }


}
