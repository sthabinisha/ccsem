package com.ccsem;

import com.ccsem.common.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static com.ccsem.common.Data.*;
/*This project is developed using JAVA swing. It is the copy of ATRAN which was provided as the HARD COPY.
The project consist of 7 ATRAN file which is then converted as the one file though the work has been
* defined in different class respective to different ATRAN. Once it is run, it ask for three file
(mag250/50 and name file) if this is not available project uses the file which is inside this file. Once it is run submit has to be pressed
and process ATRAN to finish loading the project. Once it is done the project creates two file as the output i.e comboash and conboAtran1.PRN*/
public class App extends JFrame{
    private JButton getMagFile;
    private JPanel panelMain;
    private JPanel panelTop;
    private JPanel panelMid;
    private JTextField username;
    private JButton submitButton;
    private JButton browseButton;
    private JTextField fundNo;
    private JTextField mag50Text;
    private JTextField mag250text;
    private JTextField nameprnText;
    private JButton mag50;
    private JButton mag250;
    private JButton nameprn;
    private JPanel SecondMain;
    private JPanel Front;
    private JPanel second;
    private JPanel panel3;
    private JTextField boilerTypeText;
    private JButton processAtranButton;
    private JCheckBox ATRANPredictionsCheckBox;


    private static float[][][] myarr;
    public static float[][][] freq = new float[10][8][34];
    public static int  count = 0;


    public static float[][][] myarray = new float[10][8][34];

    public static ArrayList<CCSEMStorageUnit> magnif1 = new ArrayList<>();
    public static ArrayList<CCSEMStorageUnit> magnif2 = new ArrayList<>();



    public static int  frame, point = 0;
    public static float frager, frag1, frag2, frag3, frag4, frag5, frag6, frag7, frag9, frag10, newArea, newDiameter, normSum, temp, na, mg, al, si, p, s, cl, k, ca, fe, ba, ti, area, avgDiameter;
    public static String boilerType;


    public static String  cekboxResult;
    ;
    public static String[][][] data = new String[5][3][15];


    public static ArrayList<CCSEMStorageUnit> storagePhase = new ArrayList<>();

    public static double  coalArea = 1155197.661;
    ;




    public static Random randum;


    public static float diaRatio;
//    public static FileIO fileio;
    public static float coalEsce = 0;
    public static int[] excludedCount = new int [34];
    public static float[][] tot ;
    public static float[][] totf  = new float[9][8];
    public static int[] maxFrame = {0, 0, 0, 0, 0, 0};
    public static int i = 0, blend, numMag = 2;
    public static File comboAshFile;
    public static CompoundName compoundName;
    public static GetPhase getPhase;
    public static FileIO fileio ;
    JFileChooser fileChooser = new JFileChooser();


    public App(float[][][] myarrays, float[][] totars, float[] sizeNumber) {
        add(panelMain);
        setTitle("ATRAN");
        setSize(500, 500);
        setLocationRelativeTo(null);
         fileio = new FileIO();
        getPhase = new GetPhase();
        randum = new Random();
        compoundName = new CompoundName();
        //storageUnits = new CCSEMStorageUnit();

        MagnificationServer magnificationServer = new MagnificationServer();

        this.myarr = myarrays;
        this.myarray = myarrays;
        this.tot = totars;

        mag250.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                float outVolume;
                if(fileChooser.showOpenDialog(null)==JFileChooser.APPROVE_OPTION){


                    mag250text.setText(fileChooser.getSelectedFile().getAbsolutePath());
                    File file = fileChooser.getSelectedFile();
                    magnificationServer.initializeMag250Server(new File("magnification2.xlsx"));


                    magnif2 = (ArrayList<CCSEMStorageUnit>) magnificationServer.getMag250();




                }

            }
        });
        mag50.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                float outVolume;

                if(fileChooser.showOpenDialog(null)==JFileChooser.APPROVE_OPTION){
                    mag50Text.setText(fileChooser.getSelectedFile().getAbsolutePath());
                    File file = fileChooser.getSelectedFile();
                    magnificationServer.initializeMag50Server(new File("magnification1.xlsx"));

                    magnif1 = (ArrayList<CCSEMStorageUnit>) magnificationServer.getMag50();


                }

            }


        });
        nameprn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(fileChooser.showOpenDialog(null)==JFileChooser.APPROVE_OPTION){
                    File file = fileChooser.getSelectedFile();
                    nameprnText.setText(fileChooser.getSelectedFile().getAbsolutePath());
                    magnificationServer.readNameFile(new File("Name.xlsx"));
                    magnificationServer.readNameFile(file);
                }
            }
        });
        ATRANPredictionsCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (ATRANPredictionsCheckBox.isSelected()) {
                    cekboxResult = "selected";

                }else{
                    cekboxResult ="not_selected";
                }
            }


        });

        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        setLocationRelativeTo(null);
        setVisible(true);

        if(magnif1.size()==0 && magnif2.size() == 0) {
            magnificationServer.readNameFile(new File("Name.xlsx"));
            magnificationServer.initializeMag50Server(new File("magnification1.xlsx"));
            magnif1 = (ArrayList<CCSEMStorageUnit>) magnificationServer.getMag50();
            magnif2 = (ArrayList<CCSEMStorageUnit>) magnificationServer.getMag250();
            magnificationServer.initializeMag250Server(new File("magnification2.xlsx"));
             }

        //temp


        submitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                submitButton.setText("loading...");
                new SwingWorker<Void, String>() {
                    @Override
                    protected Void doInBackground() throws Exception {
                        // Worken hard or hardly worken...
                        if(magnif1.size()==0 || magnif2.size()==0 ){
                            Thread.sleep(5000);
                        }

                        for (int i = 0; i < magnif1.size(); i++) {
                            magnif1.get(i).setPhase(getPhase.getPhase(magnif1.get(i).getAl(), magnif1.get(i).getBa(), magnif1.get(i).getCa(), magnif1.get(i).getCl(), magnif1.get(i).getFe(), magnif1.get(i).getK(), magnif1.get(i).getMg(), magnif1.get(i).getNA(), magnif1.get(i).getP(), magnif1.get(i).getS(), magnif1.get(i).getSi(), magnif1.get(i).getTi()));
                            avgDiameter = (float) (magnif1.get(i).getAvgDiameter() * Math.pow(magnificationServer.getNameData().get(i).getMyarray(), (1 / 2)));
                            float outVolume = fileio.volumeSphere(avgDiameter / 2);
                            findSize(avgDiameter, magnif1.get(i).getArea(), outVolume, (int) magnif1.get(i).getPhase(), (int) magnif1.get(i).getLocLib(), 1);

                        }
                        for (int i = 0; i < magnif2.size(); i++) {
                            magnif2.get(i).setPhase(getPhase.getPhase(magnif2.get(i).getAl(), magnif2.get(i).getBa(), magnif2.get(i).getCa(), magnif2.get(i).getCl(), magnif2.get(i).getFe(), magnif2.get(i).getK(), magnif2.get(i).getMg(), magnif2.get(i).getNA(), magnif2.get(i).getP(), magnif2.get(i).getS(), magnif2.get(i).getSi(), magnif2.get(i).getTi()));
                            avgDiameter = (float) (magnif2.get(i).getAvgDiameter() * Math.pow(magnificationServer.getNameData().get(i).getMyarray(), (1 / 2)));
                            float outVolume = fileio.volumeSphere(avgDiameter / 2);
                            findSize(avgDiameter, magnif2.get(i).getArea(), outVolume, (int) magnif2.get(i).getPhase(), (int) magnif2.get(i).getLocLib(), 2);

                        }
                        matFil(coalArea, 1, magnificationServer.getNameData());
                        boilerType = boilerTypeText.getText();

                        if(boilerType.equals(""))
                            boilerType = "Pulverized";


                        if (cekboxResult == "selected") {
                            fileio.tprintf(sumFile, "Summary of CCSEM results: PROG VERSION 2BF " );
                            fileio.tprintf(sumFile, "SAMPLE DESCRIPTION ----> " + username.getText());
                            fileio.tprintf(sumFile, "SUBMITTER          ----> " + username.getText());
                            fileio.tprintf(sumFile, "ICC # AND FUND #   ----> " + fundNo.getText());
                            fileio.tprintf(sumFile, "RUN DATE AND TIME  ----> " + dateFormat.format(now));
                        }


                        if (boilerType.equals("Fluidized")) {
                            frag1 = 20; //PYRITE
                            frag2 = 20;// IRON CARBONATE
                            frag3 = 20;// PYRRHOTITE & OX. PYRRHOTITE
                            frag4 = 1;//GYPSUM
                            frag5 = 1; // BARITE
                            frag6 = 1; //CA-AL-P
                            frag7 = 30; //KAOLINITE
                            frag9 = 9; // CALCITE & DOLOMITE
                            frag10 = 15; // QUATZ
                            coalEsce = 70;
                        } else if (boilerType.equals("Pulverized")) {
                            frag1 = 5; //PYRITE
                            frag2 = 5;// IRON CARBONATE
                            frag3 = 5;// PYRRHOTITE & OX. PYRRHOTITE
                            frag4 = 1;//GYPSUM
                            frag5 = 1;// BARITE
                            frag6 = 1;//CA-AL-P
                            frag7 = 70; //KAOLINITE
                            frag9 = 1; // CALCITE & DOLOMITE
                            frag10 = 150;// QUATZ
                            coalEsce = 150;
                        } else if (boilerType.equals("Low")) {
                            frag1 = 7;//PYRITE
                            frag2 = 7;// IRON CARBONATE
                            frag3 = 7;// PYRRHOTITE & OX. PYRRHOTITE
                            frag4 = 1;//GYPSUM
                            frag5 = 1;// BARITE
                            frag6 = 1;//CA-AL-P
                            frag7 = 20; //KAOLINITE
                            frag9 = 1;// CALCITE & DOLOMITE
                            frag10 = 25;// QUATZ
                            coalEsce = 240;
                        } else if (boilerType.equals("Cyclone")) {
                            frag1 = 350;//PYRITE
                            frag2 = 350;// IRON CARBONATE
                            frag3 = 390;// PYRRHOTITE & OX. PYRRHOTITE
                            frag4 = 1;//GYPSUM
                            frag5 = 1;// BARITE
                            frag6 = 1;//CA-AL-P
                            frag7 = 100; //KAOLINITE
                            frag9 = 1;// CALCITE & DOLOMITE
                            frag10 = 1;// QUATZ
                            coalEsce = 420;
                        } else {
                            System.out.println("No Boiler type found ");

                        }

                        writeIntoFirstFile( username.getText(), fundNo.getText(), boilerType, dateFormat.format(now),
                                magnificationServer.getNameData(),
                                magnificationServer.getNameSizeData());
                        return null;
                    }




                    @Override
                    protected void done() {

                        if (cekboxResult!="selected") {
                            JOptionPane.showMessageDialog(panelMain,
                                    "File has been exported",
                                    "Exported",
                                    JOptionPane.INFORMATION_MESSAGE
                                    );
                            System.exit(0);


                        } else {

                            Front.setVisible(false);
                            second.setVisible(true);
                            processAtranButton.addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
//
                                    ProcessAtran processAtran = new ProcessAtran();


                                    processAtran.AtranProcess( coalEsce, username.getText(), fundNo.getText(), boilerType, magnificationServer, sizeNumber);

                                    JOptionPane.showMessageDialog(panelMain,
                                            "File has been exported",
                                            "Exported",
                                            JOptionPane.INFORMATION_MESSAGE
                                    );
                                    System.exit(0);

                                }
                            });
                        }
                    }
                }.execute();



                }



        });

    }



/* The data is added in the firstFile and it is more manual coding as it is organized based on the original documentation --Binisha*/

    private void writeIntoFirstFile(String user, String fundNoText, String boilerType, String date, List<NameData> nameData, List<NameSizeData> nameSizeData) {


        fileio.tprintf(firstFile, "Summary of CCSEM results:" );
        fileio.tprintf(firstFile, "SAMPLE DESCRIPTION ----> " + user);
        fileio.tprintf(firstFile, "SUBMITTER          ----> " + user);
        fileio.tprintf(firstFile, "ICC # AND FUND #   ----> " + fundNoText);
        fileio.tprintf(firstFile, "RUN DATE AND TIME  ----> " + date);
        fileio.tprintf(firstFile, "SUMMARY OF PARAMETERS " );

        storagePhase.addAll(magnif1);
        storagePhase.addAll(magnif2);



        HashMap<Integer, List<Integer>> hashmap = new HashMap<Integer, List<Integer>>();
            HashMap<Integer, List<Integer>> hashmapM = new HashMap<Integer, List<Integer>>();

            List<Integer> curVal = hashmap.get(storagePhase.get(i).getPhase());
            HashMap<Integer, List<Integer>> hashmap1 = new HashMap<Integer, List<Integer>>();
            HashMap<Integer, List<Integer>> hashmap1M = new HashMap<Integer, List<Integer>>();

            HashMap<Integer, List<Integer>> hashmap0 = new HashMap<Integer, List<Integer>>();
            HashMap<Integer, List<Integer>> hashmap0M = new HashMap<Integer, List<Integer>>();


            List<Integer> curVal1 = hashmap.get(storagePhase.get(i).getPhase());

            List<Integer> curVal0 = hashmap.get(storagePhase.get(i).getPhase());
            fileio.tprintf(firstFile, "\n\n\n");
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
            LocalDateTime now = LocalDateTime.now();
            fileio.tprintf(firstFile, "Summary of parameters " + dtf.format(now));
            fileio.tprintf(firstFile, String.format("%1$" + 18 + "s", " ") + "  " + String.format("%1$" + 7 + "s", "NA") + "  " + String.format("%1$" + 7 + "s", "MG") + "  " + String.format("%1$" + 7 + "s", "AL") + "  " + String.format("%1$" + 7 + "s", "SI") + "  " + String.format("%1$" + 7 + "s", "P") + "  " + String.format("%1$" + 7 + "s", "S") + "  " + String.format("%1$" + 7 + "s", "CL") + "  " + String.format("%1$" + 7 + "s", "K2") + "  " + String.format("%1$" + 7 + "s", "CAO") + "  " + String.format("%1$" + 7 + "s", "FE") + "  " + String.format("%1$" + 7 + "s", "BA") + "  " + String.format("%1$" + 7 + "s", "TI"));


            for (int i = 0; i < storagePhase.size(); i++) {


                if (hashmap.containsKey((int) storagePhase.get(i).getPhase())) {
                    hashmap.get((int) storagePhase.get(i).getPhase()).add(i);
                } else {
                    curVal = new ArrayList<Integer>();
                    curVal.add(i);
                    hashmap.put((int) storagePhase.get(i).getPhase(), curVal);
                }
                if (storagePhase.get(i).getLocLib() == 1) {
                    if (hashmap1.containsKey((int) storagePhase.get(i).getPhase())) {
                        hashmap1.get((int) storagePhase.get(i).getPhase()).add(i);
                    } else {
                        curVal1 = new ArrayList<Integer>();
                        curVal1.add(i);
                        hashmap1.put((int) storagePhase.get(i).getPhase(), curVal1);
                    }
                } else {
                    if (hashmap0.containsKey((int) storagePhase.get(i).getPhase())) {
                        hashmap0.get((int) storagePhase.get(i).getPhase()).add(i);
                    } else {
                        curVal0 = new ArrayList<Integer>();
                        curVal0.add(i);
                        hashmap0.put((int) storagePhase.get(i).getPhase(), curVal0);
                    }
                }


            }


            for (Map.Entry<Integer, List<Integer>> entry : hashmap.entrySet()) {

                float sumNa = 0, sumMg = 0, sumAl = 0, sumsi = 0, sumP = 0, sumS = 0, sumCl = 0, sumCa = 0, sumK = 0, sumFe = 0, sumBa = 0, sumTi = 0;

                for (int j = 0; j < entry.getValue().size(); j++) {

                    sumNa += storagePhase.get(entry.getValue().get(j)).getNA();
                    sumMg += storagePhase.get(entry.getValue().get(j)).getMg();
                    sumAl += storagePhase.get(entry.getValue().get(j)).getAl();
                    sumsi += storagePhase.get(entry.getValue().get(j)).getSi();
                    sumS += storagePhase.get(entry.getValue().get(j)).getS();
                    sumBa += storagePhase.get(entry.getValue().get(j)).getBa();
                    sumP += storagePhase.get(entry.getValue().get(j)).getP();
                    sumK += storagePhase.get(entry.getValue().get(j)).getK();
                    sumTi += storagePhase.get(entry.getValue().get(j)).getTi();
                    sumFe += storagePhase.get(entry.getValue().get(j)).getFe();
                    sumCa += storagePhase.get(entry.getValue().get(j)).getCa();
                    sumCl += storagePhase.get(entry.getValue().get(j)).getCl();
                }
                sumNa /= entry.getValue().size();
                sumMg /= entry.getValue().size();
                sumAl /= entry.getValue().size();
                sumsi /= entry.getValue().size();
                sumS /= entry.getValue().size();
                sumBa /= entry.getValue().size();
                sumP /= entry.getValue().size();
                sumK /= entry.getValue().size();
                sumTi /= entry.getValue().size();
                sumFe /= entry.getValue().size();
                sumCa /= entry.getValue().size();
                sumCl /= entry.getValue().size();
                float norm = sumNa + sumMg + sumAl + sumsi + sumS + sumBa + sumP + sumK + sumTi + sumFe + sumCa + sumCl;
                float temps;
                if (norm != 0)
                    temps = 100 / norm;
                else
                    temps = 100;

                sumNa *= temps;

                sumMg *= temps;
                sumAl *= temps;
                sumsi *= temps;
                sumS *= temps;
                sumBa *= temps;
                sumP *= temps;
                sumK *= temps;
                sumTi *= temps;
                sumFe *= temps;
                sumCa *= temps;
                sumCl *= temps;
                fileio.tprintf(firstFile, String.format("%1$" + 20 + "s", compoundName.Name(entry.getKey())) + " " + String.format("%1$" + 7 + "s", df.format(sumNa)) + "  " + String.format("%1$" + 7 + "s", df.format(sumMg)) + "  " + String.format("%1$" + 7 + "s", df.format(sumAl)) + "  " + String.format("%1$" + 7 + "s", df.format(sumsi)) + "  " + String.format("%1$" + 7 + "s", df.format(sumP)) + "  " + String.format("%1$" + 7 + "s", df.format(sumS)) + "  " + String.format("%1$" + 7 + "s", df.format(sumCl)) + "  " + String.format("%1$" + 7 + "s", df.format(sumK)) + "  " + String.format("%1$" + 7 + "s", df.format(sumCa)) + "  " + String.format("%1$" + 7 + "s", df.format(sumFe)) + "  " + String.format("%1$" + 7 + "s", df.format(sumBa)) + "  " + String.format("%1$" + 7 + "s", df.format(sumTi)));

            }


            fileio.tprintf(firstFile, "\n\nSummary of liberated ");
            fileio.tprintf(firstFile, String.format("%1$" + 18 + "s", " ") + "  " + String.format("%1$" + 7 + "s", "NA") + "  " + String.format("%1$" + 7 + "s", "MG") + "  " + String.format("%1$" + 7 + "s", "AL") + "  " + String.format("%1$" + 7 + "s", "SI") + "  " + String.format("%1$" + 7 + "s", "P") + "  " + String.format("%1$" + 7 + "s", "S") + "  " + String.format("%1$" + 7 + "s", "CL") + "  " + String.format("%1$" + 7 + "s", "K2") + "  " + String.format("%1$" + 7 + "s", "CAO") + "  " + String.format("%1$" + 7 + "s", "FE") + "  " + String.format("%1$" + 7 + "s", "BA") + "  " + String.format("%1$" + 7 + "s", "TI")/*+ "  " + String.format("%1$" + 10 + "s", "Excluded")*/);


            for (Map.Entry<Integer, List<Integer>> entry : hashmap1.entrySet()) {
//            for(int i = 1; i<= 33; i++){
//                if(hashmap.containsKey(i)) {
                float sumNa = 0, sumMg = 0, sumAl = 0, sumsi = 0, sumP = 0, sumS = 0, sumCl = 0, sumCa = 0, sumK = 0, sumFe = 0, sumBa = 0, sumTi = 0;

                for (int j = 0; j < entry.getValue().size(); j++) {

                    sumNa += storagePhase.get(entry.getValue().get(j)).getNA();
                    sumMg += storagePhase.get(entry.getValue().get(j)).getMg();
                    sumAl += storagePhase.get(entry.getValue().get(j)).getAl();
                    sumsi += storagePhase.get(entry.getValue().get(j)).getSi();
                    sumS += storagePhase.get(entry.getValue().get(j)).getS();
                    sumBa += storagePhase.get(entry.getValue().get(j)).getBa();
                    sumP += storagePhase.get(entry.getValue().get(j)).getP();
                    sumK += storagePhase.get(entry.getValue().get(j)).getK();
                    sumTi += storagePhase.get(entry.getValue().get(j)).getTi();
                    sumFe += storagePhase.get(entry.getValue().get(j)).getFe();
                    sumCa += storagePhase.get(entry.getValue().get(j)).getCa();
                    sumCl += storagePhase.get(entry.getValue().get(j)).getCl();
                }
                sumNa /= entry.getValue().size();
                sumMg /= entry.getValue().size();
                sumAl /= entry.getValue().size();
                sumsi /= entry.getValue().size();
                sumS /= entry.getValue().size();
                sumBa /= entry.getValue().size();
                sumP /= entry.getValue().size();
                sumK /= entry.getValue().size();
                sumTi /= entry.getValue().size();
                sumFe /= entry.getValue().size();
                sumCa /= entry.getValue().size();
                sumCl /= entry.getValue().size();
                float norm = sumNa + sumMg + sumAl + sumsi + sumS + sumBa + sumP + sumK + sumTi + sumFe + sumCa + sumCl;
                float temps;
                if (norm != 0)
                    temps = 100 / norm;
                else
                    temps = 100;

                sumNa *= temps;

                sumMg *= temps;
                sumAl *= temps;
                sumsi *= temps;
                sumS *= temps;
                sumBa *= temps;
                sumP *= temps;
                sumK *= temps;
                sumTi *= temps;
                sumFe *= temps;
                sumCa *= temps;
                sumCl *= temps;
                fileio.tprintf(firstFile, String.format("%1$" + 20 + "s", compoundName.Name(entry.getKey())) + " " + String.format("%1$" + 7 + "s", df.format(sumNa)) + "  " + String.format("%1$" + 7 + "s", df.format(sumMg)) + "  " + String.format("%1$" + 7 + "s", df.format(sumAl)) + "  " + String.format("%1$" + 7 + "s", df.format(sumsi)) + "  " + String.format("%1$" + 7 + "s", df.format(sumP)) + "  " + String.format("%1$" + 7 + "s", df.format(sumS)) + "  " + String.format("%1$" + 7 + "s", df.format(sumCl)) + "  " + String.format("%1$" + 7 + "s", df.format(sumK)) + "  " + String.format("%1$" + 7 + "s", df.format(sumCa)) + "  " + String.format("%1$" + 7 + "s", df.format(sumFe)) + "  " + String.format("%1$" + 7 + "s", df.format(sumBa)) + "  " + String.format("%1$" + 7 + "s", df.format(sumTi))/*+ "  " +String.format("%1$" + 10 + "s", df.format(entry.getValue().size()* 100/33))*/);


            }


            fileio.tprintf(firstFile, "\n\nSummary of locked ");
            fileio.tprintf(firstFile, String.format("%1$" + 18 + "s", " ") + "  " + String.format("%1$" + 7 + "s", "NA") + "  " + String.format("%1$" + 7 + "s", "MG") + "  " + String.format("%1$" + 7 + "s", "AL") + "  " + String.format("%1$" + 7 + "s", "SI") + "  " + String.format("%1$" + 7 + "s", "P") + "  " + String.format("%1$" + 7 + "s", "S") + "  " + String.format("%1$" + 7 + "s", "CL") + "  " + String.format("%1$" + 7 + "s", "K2") + "  " + String.format("%1$" + 7 + "s", "CAO") + "  " + String.format("%1$" + 7 + "s", "FE") + "  " + String.format("%1$" + 7 + "s", "BA") + "  " + String.format("%1$" + 7 + "s", "TI")/*+ "  " + String.format("%1$" + 10 + "s", "Excluded")*/);

            for (Map.Entry<Integer, List<Integer>> entry : hashmap0.entrySet()) {
//            for(int i = 1; i<= 33; i++){
//                if(hashmap.containsKey(i)) {
                float sumNa = 0, sumMg = 0, sumAl = 0, sumsi = 0, sumP = 0, sumS = 0, sumCl = 0, sumCa = 0, sumK = 0, sumFe = 0, sumBa = 0, sumTi = 0;

                for (int j = 0; j < entry.getValue().size(); j++) {

                    sumNa += storagePhase.get(entry.getValue().get(j)).getNA();
                    sumMg += storagePhase.get(entry.getValue().get(j)).getMg();
                    sumAl += storagePhase.get(entry.getValue().get(j)).getAl();
                    sumsi += storagePhase.get(entry.getValue().get(j)).getSi();
                    sumS += storagePhase.get(entry.getValue().get(j)).getS();
                    sumBa += storagePhase.get(entry.getValue().get(j)).getBa();
                    sumP += storagePhase.get(entry.getValue().get(j)).getP();
                    sumK += storagePhase.get(entry.getValue().get(j)).getK();
                    sumTi += storagePhase.get(entry.getValue().get(j)).getTi();
                    sumFe += storagePhase.get(entry.getValue().get(j)).getFe();
                    sumCa += storagePhase.get(entry.getValue().get(j)).getCa();
                    sumCl += storagePhase.get(entry.getValue().get(j)).getCl();
                }
                sumNa /= entry.getValue().size();
                sumMg /= entry.getValue().size();
                sumAl /= entry.getValue().size();
                sumsi /= entry.getValue().size();
                sumS /= entry.getValue().size();
                sumBa /= entry.getValue().size();
                sumP /= entry.getValue().size();
                sumK /= entry.getValue().size();
                sumTi /= entry.getValue().size();
                sumFe /= entry.getValue().size();
                sumCa /= entry.getValue().size();
                sumCl /= entry.getValue().size();
                float norm = sumNa + sumMg + sumAl + sumsi + sumS + sumBa + sumP + sumK + sumTi + sumFe + sumCa + sumCl;
                float temps;
                if (norm != 0)
                    temps = 100 / norm;
                else
                    temps = 100;

                sumNa *= temps;

                sumMg *= temps;
                sumAl *= temps;
                sumsi *= temps;
                sumS *= temps;
                sumBa *= temps;
                sumP *= temps;
                sumK *= temps;
                sumTi *= temps;
                sumFe *= temps;
                sumCa *= temps;
                sumCl *= temps;
                fileio.tprintf(firstFile, String.format("%1$" + 20 + "s", compoundName.Name(entry.getKey())) + " " + String.format("%1$" + 7 + "s", df.format(sumNa)) + "  " + String.format("%1$" + 7 + "s", df.format(sumMg)) + "  " + String.format("%1$" + 7 + "s", df.format(sumAl)) + "  " + String.format("%1$" + 7 + "s", df.format(sumsi)) + "  " + String.format("%1$" + 7 + "s", df.format(sumP)) + "  " + String.format("%1$" + 7 + "s", df.format(sumS)) + "  " + String.format("%1$" + 7 + "s", df.format(sumCl)) + "  " + String.format("%1$" + 7 + "s", df.format(sumK)) + "  " + String.format("%1$" + 7 + "s", df.format(sumCa)) + "  " + String.format("%1$" + 7 + "s", df.format(sumFe)) + "  " + String.format("%1$" + 7 + "s", df.format(sumBa)) + "  " + String.format("%1$" + 7 + "s", df.format(sumTi))/*+ "  " +String.format("%1$" + 10 + "s", df.format(entry.getValue().size()* 100/33))*/);


            }


            //here

            fileio.tprintf(firstFile, "\n\nSUMMARY OF WEIGHT PERCENT ON MINERAL BASIS  " );
            fileio.tprintf(firstFile,"RUN DATE AND TIME " + dtf.format(now));
            fileio.tprintf(firstFile, "PERCENT EPOXY USED      " + "50");
            fileio.tprintf(firstFile, "TOTAL MINERAL AREA ANALYZED    ----> " + tot[1][7]);
            fileio.tprintf(firstFile, "FIELD SIZE USED                 ----> " +  Math.pow((((((Math.sqrt(115538.8) + 10.0) / 4.167) * 1000) / 240) - (2 * 5)), 2));
            fileio.tprintf(firstFile, "NUMBER OF FRAMES                ----> " + 2+ "\n\n");


            fileio.tprintf(firstFile, String.format("%1$" + 27 + "s", "1.0") + "    2.2     4.6    10    22      46  ");
            fileio.tprintf(firstFile, String.format("%1$" + 27 + "s", "TO") + "      TO     TO     TO    TO     TO    TOTALS   EXCLUDED");
            fileio.tprintf(firstFile, String.format("%1$" + 27 + "s", "2.2") + "     4.6     10     22    46     100");
            fileio.tprintf(firstFile, "-----------------------------------------------------------------------------------------\n");
            for (int i = 1; i <= 33; i++) {
                fileio.tprintfn(firstFile, String.format("%1$" + 20 + "s", nameData.get(i-1).getNameselement()));
                fileio.tprintf(firstFile, "  " + String.format("%1$" + 5 + "s", df.format(myarr[4][1][i])) + "  " + String.format("%1$" + 5 + "s", df.format(myarr[4][2][i])) + "  " + String.format("%1$" + 5 + "s", df.format(myarr[4][3][i])) + "  " + String.format("%1$" + 5 + "s", df.format(myarr[4][4][i])) + "  " + String.format("%1$" + 5 + "s", df.format(myarr[4][5][i])) + "  " + String.format("%1$" + 5 + "s", df.format(myarr[4][6][i])) + "  " + String.format("%1$" + 5 + "s", df.format(myarr[4][7][i]))+ "  " + String.format("%1$" + 5 + "s", df.format(excludedCount[i]/myarr[4][7][i])));


            }

        fileio.tprintf(firstFile, "-----------------------------------------------------------------------------------------\n");
        fileio.tprintf(firstFile, String.format("%1$"+20+ "s", "TOTALS")+ "   "  + String.format("%1$"+5+ "s",df.format(tot[4][1] ))+"  "+String.format("%1$"+5+ "s",df.format(tot[4][2] )) +"  "+String.format("%1$"+5+ "s",df.format(tot[4][3] )) +"  "+ String.format("%1$"+5+ "s",df.format(tot[4][4]) )+"  "+String.format("%1$"+5+ "s",df.format(tot[4][5] ))+"  "+String.format("%1$"+5+ "s",df.format(tot[4][6] ))+"  "+String.format("%1$"+5+ "s",df.format(tot[4][7]) ) );
        fileio.tprintf(firstFile, "-----------------------------------------------------------------------------------------\n");

        fileio.tprintf(firstFile, "\n\nSUMMARY OF FREQUENCY PERCENT ON MINERAL BASIS  " );


        fileio.tprintf(firstFile, String.format("%1$" + 27 + "s", "1.0") + "    2.2     4.6    10    22      46  ");
        fileio.tprintf(firstFile, String.format("%1$" + 27 + "s", "TO") + "      TO     TO     TO    TO     TO    TOTALS  ");
        fileio.tprintf(firstFile, String.format("%1$" + 27 + "s", "2.2") + "     4.6     10     22    46     100");
        fileio.tprintf(firstFile, "-----------------------------------------------------------------------------------------\n");

        for (int i = 1; i <= 33; i++) {
            fileio.tprintfn(firstFile, String.format("%1$" + 20 + "s", nameData.get(i-1).getNameselement()));
            float tot = 100/(freq[1][1][i]+freq[1][2][i]+ freq[1][3][i]+ freq[1][4][i]+ freq[1][5][i]+freq[1][6][i]+freq[1][7][i]);
            fileio.tprintf(firstFile, "  " + String.format("%1$" + 5 + "s", df.format(freq[1][1][i])) + "  " + String.format("%1$" + 5 + "s", df.format(freq[1][2][i])) + "  " + String.format("%1$" + 5 + "s", df.format(freq[1][3][i])) + "  " + String.format("%1$" + 5 + "s", df.format(freq[1][4][i])) + "  " + String.format("%1$" + 5 + "s", df.format(freq[1][5][i])) + "  " + String.format("%1$" + 5 + "s", df.format(freq[1][6][i])) + "  " + String.format("%1$" + 5 + "s", df.format(freq[1][7][i])));


        }
        fileio.tprintf(firstFile, "-----------------------------------------------------------------------------------------\n");
        fileio.tprintf(firstFile, String.format("%1$"+20+ "s", "TOTALS")+ "   "  + String.format("%1$"+5+ "s",df.format(totf[1][1] ))+"  "+String.format("%1$"+5+ "s",df.format(totf[1][2] )) +"  "+String.format("%1$"+5+ "s",df.format(totf[1][3] )) +"  "+ String.format("%1$"+5+ "s",df.format(totf[1][4]) )+"  "+String.format("%1$"+5+ "s",df.format(totf[1][5] ))+"  "+String.format("%1$"+5+ "s",df.format(totf[1][6] ))+"  "+String.format("%1$"+5+ "s",df.format(totf[1][7]) ) );
        fileio.tprintf(firstFile, "-----------------------------------------------------------------------------------------\n");






    }



    private static void findSize(float diam, float area, float outVolume, int phasen, int locLib, int m) {
        int category;
        if (diam < 2.2)
            category = 1;
        else if (diam < 4.6)
            category = 2;
        else if (diam < 10)
            category = 3;
        else if (diam < 22)
            category = 4;
        else if (diam < 46)
            category = 5;
        else
            category = 6;
        myarr[1][category][phasen] += outVolume;
        myarr[1][7][phasen] += outVolume;
        myarr[2][category][phasen] += 1;
        myarr[2][7][phasen] += 1;
        excludedCount[phasen]+= locLib;
        freq[1][category][phasen] += 1;
        //freq[2][category][phasen] += 1;
         freq[1][7][phasen] += 1;

        count++;



    }

    /*
     * Material Fill
     *
     * Description:
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 1994
     * Re-Modified by Binisha and copied in Java format
     * */
    private static void matFil(double coalArea, int cf, List<NameData> magnificationServer) {
        float totalArea, totalDensity, sum;
        double weightCoalArea;

        for (int i = 1 ; i <= 33 ; ++i) {
            for (int j = 1; j <= 6; ++j) {
                if (j <= 3) {
                    myarr[7][j][i] = myarr[1][j][i] * cf;

                } else {
                    myarr[7][j][i] = myarr[1][j][i];

                }
                if( freq[1][j][i] != 0) {
                    //freq[1][j][i] = (freq[1][j][i] / freq[1][7][i]  ) * 100;

                }
                myarr[9][2][1] += myarr[7][j][i]; //totaling mineral areas
                myarr[9][3][1] += myarr[7][j][i] * magnificationServer.get(i-1).getMyarray(); //area * density
                myarr[9][1][i] = (float) magnificationServer.get(i-1).getMyarray();;

                myarr[7][7][i] += myarr[7][j][i];
                freq[2][1][1] +=freq[1][j][i];

            }
        }
        weightCoalArea = coalArea - myarr[9][2][1];

        //excluded: liberated
        totalArea = myarr[9][2][1];
        totalDensity= myarr[9][3][1];


        sum = (float) (totalDensity + ( weightCoalArea * 1.4));
        for (int j = 1; j <=33 ; j++) {
            for (int k = 1; k <=6 ; k++) {
                myarr[3][k][j] = ( myarr[7][k][j] /totalArea) * 100;
                myarr[4][k][j] = (float) ((( myarr[7][k][j] *  magnificationServer.get(j-1).getMyarray())/totalDensity) * 100);
                myarr[5][k][j] = (float) (( myarr[7][k][j] /coalArea) * 100);
                myarr[6][k][j] = (float) ((( myarr[7][k][j] *  magnificationServer.get(j-1).getMyarray())/ sum) * 100);




                if( myarr[7][7][j] != 0){
                    myarr[8][k][j] = myarr[7][k][j] / myarr[7][7][j] * 100;

                }
                if( freq[1][7][j] != 0){
                    freq[8][k][j] = freq[7][k][j] /freq[2][1][1] * 100;

                }
            }
        }

        for (int i = 1; i <= 33 ; i++) {
            for (int j = 3; j <=6 ; j++) {
                for (int k = 1; k <=6 ; k++) {
                    myarr[j][7][i] += myarr[j][k][i];
                    freq[j][7][i] += freq[j][k][i];
                }
            }
        }
        for (int i = 1; i <= 33 ; i++) {
            for (int j = 1; j <=6 ; j++) {
                myarr[8][7][i] += myarr[8][j][i];

            }
        }
        for (int i = 1; i <= 33 ; i++) {
            for (int j = 1; j <=7 ; j++) {
                for (int k = 1; k <=7 ; k++) {
                    tot[j][k] += myarr[j][k][i];

                }
            }
        }
        for (int i = 1; i <=7 ; i++) {
            tot[8][i] = tot[3][i];

        }

        for (int i = 1; i <= 33 ; i++) {
            for (int k = 1; k <= 7; k++) {
                if (freq[1][k][i] != 0) {
                    freq[1][k][i] = (freq[1][k][i] / count) * 100;

                }
            }
        }
        for (int k = 1; k <=7 ; k++) {
            for (int i = 1; i <= 33 ; i++) {
                totf[1][k] += freq[1][k][i];


            }
        }




    }

}
