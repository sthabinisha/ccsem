package com.ccsem;

import com.ccsem.common.FileIO;
import com.ccsem.common.MagGrType;

import javax.swing.*;
import java.io.File;
import java.util.ArrayList;

import static com.ccsem.common.Data.*;

public class Main {
    public static int[] hiPartSize = new int[3];
    public static int[] loPartSize = new int[3];
    public static MagGrType magnif;/*This structure with hold the magnification and guard region*/
    public static ArrayList<MagGrType> magValue = new ArrayList<>();
    public static FileIO fileIO;
    public static int[] excludedCount = new int [34];
    public static float[][] totar = new float[9][8];
    public static float[][][] myarray = new float[10][8][34];
    public static float[] sizeNumber = new float[20];
    public static int k = 2;


    public static void main(String[] args) {

         fileIO = new FileIO();
        zeroArray();
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        ReadMagnification(2);


        App myForm = new App(myarray, totar, sizeNumber);
        myForm.setVisible(true);

    }
    /*Read Magnification
     *
     * Description: "Mag.PRN" contains the following
     * 1. Number of magnification that will be used
     * 2. Magnification
     * 3. max. Particle dia will be processed. ( This is used for determining the Guard regions
     * 4. Loparticle size and Hiparticles sizes.
     *   The magnification should be specified in the decreasing order of magnification i.e highest to lowest magnification size
     *  (same as smallest to largest particles size). This size is output to a file SIZE.PRN. Later, com.ccsem.Atran2 and Atran3 reads from
     *  this file and classifies the particle according to the size bin read. As the variable "size" is not needed anywhere else
     *  in the program, it will be declared locally. for detailed description on the classification see the "programmer Notes",
     *  if you meed the following criteria:
     * 1. You have access priviledge to such information and
     * 2. you can decipher Mr. C.R's handwriting.
     *
     * Pre condn:
     * num is assigned a pointer.
     *
     * post Condn:
     * *num contains the number of magnification regions.
     * magnif contains the number of magnification regions.
     * Magnif record holds all the magnification values and their guard regions
     * Hip holds the max. particle cutoff size for tht magnification
     * Lop holds the min. particle cutoff size for that magnification
     *
     *
     * Changes : The hardcoded date is written in Size PRN  file which has been chanded and updated to sizeNumber.
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 1/23/96 hard coded in the mag.prn,
     * ATRAN only handles to magnification files and therefore no need to load mag.prn
     * Re-Modified by Binisha and copied in Java format
     *
     * Company: Energy & Environmental Research Center
     *
     *  /
     */
    private static void ReadMagnification(int numMag) {
        // TODO Auto-generated method stub

        float[][] size = new float[3][7];// changed size[21] to size[3][7] 10/18/95 SEA
        /*240 high magnification 1-10um
         * 50 low magnification 10-50um*/
        int[] magnifValue = {240, 50};
        int[] maxSize = {10 / 2, 100 / 2};
        hiPartSize = new int[]{10, 50};// the usual limit is 100um, but large particles seems to wash out
        //the smaller samples, especially a large ca particles
        loPartSize = new int[]{1, 10};
        int num = 2;

        for (int i = 0; i < 2; i++) {
            magnif = new MagGrType();
            magnif.setMag(magnifValue[i]);
            magnif.setGr(maxSize[i]);
            magValue.add(magnif);
        }

        for (int i = 0; i < 6; i++) {
            size[0][i] = loPartSize[0] + i * (hiPartSize[0] - loPartSize[0]) / 6;
        }
        size[0][6] = hiPartSize[0];


        if (num > 1) {
            size[1][1] = (float) (hiPartSize[0] + loPartSize[0]) / 2;
            size[1][0] = loPartSize[0];

            for (int j = 2; j < 6; j++) {
                size[1][j] = loPartSize[1] + (j - 2) * (hiPartSize[1] - loPartSize[1]) / 4;
            }
            size[1][6] = hiPartSize[1];
        }
        if (num > 2) {
            for (int j = 3; j < 6; j++) {
                size[2][j] = loPartSize[2] + (j - 3) * (hiPartSize[2] - loPartSize[2]) / 3;
            }
            size[2][6] = hiPartSize[2];
            size[2][2] = (loPartSize[1] + hiPartSize[1]) / 2;
            size[2][1] = (loPartSize[0] + hiPartSize[0]) / 2;


            size[2][0] = loPartSize[0];
        }
        /*now sizes are sorted for each magnification range i.e 1-7, 8-14, 15-21*/
        size = SortSize(size, num);
        /*sizes are now output to the file*/
        File file1 = fileIO.createFile(sumFile);
        File file2 = fileIO.createFile(firstFile);
        if (file2.length() != 0) {
            file1.delete();
            file2.delete();
        }
//size.prn is based on the 2 magnification size.

        for (int i = 0; i < num; i++) {
            sizeNumber[0] = (float) 0.0;
            sizeNumber[1] = (float) 0.0;
            sizeNumber[k++] = (float) 0.1;
            sizeNumber[k++] = (float) 0.5;

            for (int j = 0; j < 7; j++) {
                sizeNumber[k++] = size[i][j];
            }


        }
    }//ReadMagnification
    /* SortSize
     *
     * Descriptin: Sorts size bins, making smallest size first
     *
     * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 1994
     * Re-Modified by Binisha and copied in Java format
     *
     * Company: Energy & Environmental Research Center
     * */
    private static float[][] SortSize(float[][] size, int num) {
        int i, j, k, min;
        float TSize = 0;
        // TODO Auto-generated method stub

        for (k = 0; k < num; k++) {
            for (i = 0; i < 6; i++) {
                min = i;
                for (j = i; j < 7; j++) {
                    if (size[k][j] < size[k][min]) {
                        min = j;
                    }

                }
                TSize = size[k][i];
                size[k][i] = size[k][min];
                size[k][min] = TSize;

            }

        }
        return size;

    }//SortSize
    /*
     * Atran4
     *
     * Description: Initializes MArray and total to 0
     * Marray: [Table][sizebins][1-33 composition phases]
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 1994
     * Re-Modified by Binisha and copied in Java format
     * */
    private static void zeroArray() {
        // TODO Auto-generated method stub

        for (int i = 1; i <= 9; i++) {
            for (int j = 1; j <= 7; j++) {
                for (int k = 1; k <= 33; k++) {
                    myarray[i][j][k] = (float) 0.0;

                }
            }
        }
        for (int i = 1; i <= 8; i++) {
            for (int j = 1; j <= 7; j++) {
                totar[i][j] = 0;

            }

        }
    }

}
